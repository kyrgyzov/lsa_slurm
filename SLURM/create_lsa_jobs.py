#!/usr/bin/env python

import sys, getopt, inspect, glob, os, string, time

currentDir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
rootDir = os.path.dirname(currentDir) + '/'
sys.path.insert(0,rootDir + 'LSA/') 

from lsa_job import save_data, check_system
from job_vars import JobVars
from job_code import JobCode

help_message = 'usage example: python SLURM/create_jobs.py -p ProjectName -j JobName'
if __name__ == "__main__":
	
	check_system()
	
	prj = None
	job = None

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:j:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			prj = arg
			try:
				JV = JobVars(rootDir + 'PRJ/' + prj)
			except:
				print prj +' is not known project file.'
				print 'check file in PRJ/ dir'
				print help_message
				sys.exit(2)

		elif opt in ('-j'):
			job = arg
			try:
				JC = JobCode(rootDir + 'SLURM/',job)
			except:
				print job + ' is not known job.'
				print 'check job names in SLURM/*.code files'
				print help_message
				sys.exit(2)

	if (prj is None)  or (job is None):
		print 'project name:',prj
		print 'or'
		print 'job name:',job
		print 'are not known.'
		print 'check their names'
		sys.exit(2)

	JV.add('prjID',prj)
	JV.add('jobID',job)
	JV.make_prj_space()
	JV.get_prj_info()
	JV.save()
	
        JV = JobVars(JV.oDir)

	JV.remove_old_slurm_files()

	sname,oname,ename = JV.get_slurm_job_file_name()
	
	script_code = JC.make_script(JV)

	save_data(script_code,sname,fl='str')

        print 'new ' + JV.jobID +' SLURM job was successfully created in file:'
        print sname
	
