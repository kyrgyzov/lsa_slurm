#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from lsa_stats import SafeRt,get_counts

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
from counts_vector import Counts_Vector
import warnings

help_message = 'usage example: python script_file.py -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h','--help'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	warnings.filterwarnings("ignore")

	HO = StreamingEigenhashes(JV.oDir)

        ###########

	ct = time.time()
	HI,HV = HO.get_hash_indices()
	R=[]
	R.append('max(HV)'+str(max(HV)))

	HV=np.array(HV,dtype=np.uint32)

	t1=  np.array(HV ==1 ,dtype=np.uint8)
	nbr1 = int(np.sum(t1))

	R.append('total # of hashes '+str(len(HI)))
	R.append('# of single-value hashes '+str(nbr1)+', ratio '+str(float(nbr1)/len(HI)))

	TR=['sample,# of hashes in sample,ratio to hMax pos.,ratio to hReal (non-zero col.)']
	for i in range(HO.nbr_S):
		tfn = HO.sampleHashFile[i]
		sname =  tfn[tfn.rfind('/'):]
		nzi = load_data(HO.sampleHashFile[i] + '.nzi',fl='list')
		tnbr = len(nzi)
		TR.append(sname+','+str(tnbr)+','+str(float(tnbr)/HO.hMax)+','+str(float(tnbr)/len(HI)))
	save_data(TR,JV.oDir + JV.sdrREP + 'hash/' + 'hash_sample_stat.txt',fl='str')

	nbrH = len(HI)

	if HO.filter_uq:
		nbrH -= nbr1
	
	R.append('# of hashes to save '+str(nbrH))
	smH = int(np.sum(HV))

	rHV = np.copy(HV)
	if HO.filter_uq:
		rHV = rHV[rHV>1]
		HI = [HI[i] for i in xrange(len(HI)) if not t1[i]]

	R.append('totoal # of hashes in fastqs '+str(smH))
	R.append('totoal # of non-one hashes in fastqs '+str(smH-nbr1)+', '+str(np.sum(rHV)))
	R.append('unique # of non-one hashes in fastqs '+str(rHV.size))


	save_data(HI,HO.oDir + HO.sdrCLS + 'hash_indices',fl='list')
	save_data(nbrH, HO.oDir + HO.sdrCLS + 'nbrHashes.txt',fl='nbr')
	save_data(rHV,HO.oDir + HO.sdrCLS + 'nbr_hashes',fl='mm')

	R.append('kMer - ' + JV.kMer)

	hmax = JV.hash_max()

	R.append('hMax value- ' + str(hmax))

	R.append('# of samples - ' + JV.nbr_S)

	H = get_counts(HI,hmax)

	R.append('cMax - ' + str(hmax/len(H)))
	R.append('# of chunks - ' + str(len(H)))

	fig, ax = plt.subplots()
	ax.stem(range(len(H)), H)
	fig.savefig(JV.oDir + JV.sdrREP + 'hash/' + 'hash_hist_global.png',format='png')
	plt.close(fig)

	md_i = len(HI)/2
	R.append('global by columns' + ' n-z el. - ' + str(len(HI)) + '; % of n-z el. to hash-size - ' + str(SafeRt(len(HI),hmax)) + '; median - ' + str(SafeRt(HI[md_i],hmax)))

	save_data(R,JV.oDir + JV.sdrREP + 'hash/' + 'hash_stat.txt',fl='str')

