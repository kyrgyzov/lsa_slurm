#!/usr/bin/env python
import sys,os,glob
import getopt,gzip
from lsa_job import *
from lsa_names import *
from fastq_reader import Fastq_Reader
from job_vars import JobVars


if __name__ == "__main__":
        try:
                opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
        except:
                print help_message
                sys.exit(2)

        for opt, arg in opts:
                if opt in ('-h'):
                        print help_message
                        sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	if type(FP) is not list:
		FP = [FP]
	AS,step = JV.get_array_size('files')
	FO = Fastq_Reader(JV.oDir)

	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			break
		print '---------------------------------'
		fname = FP[ind]

		FO.bipart_fa(fname)

		name = get_file_name(fname)
		nm1 = FO.oDir + FO.sdrTMP + name +'1'
		nm2 = FO.oDir + FO.sdrTMP + name +'2'
		nmf = FO.oDir + FO.sdrSAMPLES + name[:name.rfind('fa')] + 'fastq'
		
		f1 = open_file(nm1)
		f2 = open_file(nm2)
		ff = open_file(nmf,'w')
		
		line = None
		while (line != ''):
			LS = []
			line = f1.readline()
			if line:
				LS.append(line)
				LS.append(f1.readline())
				LS.append(f2.readline())
				LS.append(f2.readline())
				ff.write('@' + LS[0])
				ff.write(LS[1])
				ff.write('+\n')
				ff.write(''.join(['I']*(len(LS[1])-1))+'\n')
				ff.write('@' + LS[2])
				ff.write(LS[3])
				ff.write('+\n')
				ff.write(''.join(['I']*(len(LS[3])-1))+'\n')


		f1.close()
		f2.close()
		ff.close()
