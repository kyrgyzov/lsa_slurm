#!/usr/bin/env python

### THIS MAY OCCUPY ~10-50GB OF /tmp SPACE PER JOB

import glob,os
import signal,shutil
import sys,getopt
import gzip
import numpy as np
from collections import defaultdict
from fastq_reader import Fastq_Reader
from lsa_job import *
from job_vars import JobVars

def max_log_lik_ratio(s,bkg,h1_prob=0.8,thresh1=3.84):
	LLR = []
	rws = s[-1]
	del s[-1]
	v1 = rws*h1_prob*(1-h1_prob)
	m1 = rws*h1_prob

	if len(s) == 1:
		LLR.append((1,s.keys()[0]))
	else:
		for k,sect_sum in s.items():
			if sect_sum >= rws*bkg[k]:
				v2 = rws*bkg[k]*(1-bkg[k])
				m2 = rws*bkg[k]
				llr = np.log(v2**.5/v1**.5) + .5*((sect_sum-m2)**2/v2 - (sect_sum-m1)**2/v1)
				LLR.append((llr,k))
		LLR.sort(reverse=True)

	K = int(LLR[0][1])
	return K

help_message = 'usage example: python read_partition_parts.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	FASTQ = load_data(JV.oDir +JV.sdrJOB + 'fileList.txt',fl='str')

	save_dir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir

	numCLS = load_data(save_dir + 'nbrClusters.txt',fl='nbr') 
	print 'numCLS',numCLS

	CP = load_data(save_dir + 'cluster_probs.npy',fl='npy')
	cluster_probs = dict(enumerate(CP))

	del CP

	HW = load_data(JV.hash_weight_file(),fl='mm',dump=np.float32)
	nbr_hash  = HW.size
	print 'nbr_hash',nbr_hash

	CLS = load_data(JV.oDir + JV.sdrCLS + HO.dec_cls_dir + 'hash_cluster',fl='mm', dump = np.uint16)
	PS  = load_data(JV.oDir + JV.sdrCLS + HO.dec_cls_dir + 'hash.pos', fl='mm', dump = np.uint64)

	dvd = 2 if HO.paired_reads else 1

	sf = open_file(JV.oDir + JV.sdrREP + 'reads2class/' + HO.dec_cls_dir + 'jobs/job.' +str(fr)+ '.sts','w')
	
	for stp in range(step):
		ind = step*fr + stp
		if ind >= HO.nbr_F:
			break

		print '-----------------------'
		print 'ind',ind
		fName = FASTQ[ind]
		print 'fName',fName
		Name = get_file_name(fName)
		print 'Name',Name

		Name = cut_sufix_from_name(Name,sfx='.fastq')
		print 'Name',Name
		smpDIR = get_prefix_from_name(Name) + '/'
		print 'smpDIR',smpDIR

		infile = JV.oDir + JV.sdrHASH + smpDIR + Name +'.hashq.gz'
		sfile  = save_dir + 'SMP/' + smpDIR  + Name + '.cls'
		'''
		ok = os.path.isfile(sfile)
		if ok:
			try:
				tt = load_data(sfile)
				del tt
				ok = True
			except:
				ok = False
		if ok:
			continue
		'''
		print 'file - ',infile
		print '-----------------------'

		CI = [[] for i in xrange(numCLS)]

		r_id = 0

		HRG = HO.read_from_hashq(infile)

		D = defaultdict(float)
		for vec in HRG:
			for ci in vec:
				D[-1] += HW[ci]
				tC = CLS[PS[ci]:PS[ci+1]]
				for x in tC:
					D[x] += HW[ci]

			if (r_id + 1) % dvd == 0:
				best_clust = max_log_lik_ratio(D,cluster_probs)
				D = defaultdict(float)
				CI[best_clust].extend([r_id/dvd])
			r_id += 1

		save_data(CI,sfile,fl='list')
		readStats2File =  Name + ',' + ','.join([str(len(v)) for v in CI]) + '\n'
		sf.write(readStats2File)

		del CI
		print 'total reads processed:',r_id
		print 'saved into',sfile

	del HW,CLS,PS
	sf.close()
