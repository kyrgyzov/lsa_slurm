#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from lsa_job import *

def float_bins(V,nbr,p_filter = 0.0, mxb =1.0,cut=True):

	V = np.sort(V)
	ln = V.size

	V = V[:(int(ln*(1.0-p_filter)))]

	wd = float(mxb)/nbr
	H =[0]*nbr

	for el in V:
		ind = int(el/wd)

		if ind >=nbr:
			if cut:
				pass
			else:
				H[nbr-1] += 1
		else:
			H[ind] += 1

	return H

def get_counts(LS,mval):

	cMax = 1
	if mval >= 1024:
		cMax = mval/1024
		
	nbr_chunks = mval/cMax + 1

	H =[0]*nbr_chunks
	for el in LS:
		H[el/cMax] += 1
	return H

def SafeRt(v1,v2):
	
	try: 
		rt = float(v1)/v2
	except :
		rt = 0.0
	return rt


def file_word_len(fname,wrd =''):

	fl = len(wrd) > 0
	n = 0
	i = -1
	with open(fname) as f:
		for i, l in enumerate(f):
			if fl:
				if wrd in l:
					n += 1
	return i + 1,n


def spike_part_dist(JV,ind):

	path2save = JV.oDir + JV.sdrREP + 'parts/' + JV.dec_cls_dir()

	p2file = path2save + 'stat_prt_' + str(ind) + '.txt'
	f = open_file(p2file, 'w')

	f.write('%10s%15s%15s%15s'%('file name','# of spikes','# of reads', 'ratio'))
	f.write('\n')

	CS = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir() + 'nbrClusters.txt',fl='nbr')

	fname =str(ind) + '.fastq'
	if os.path.isdir(JV.p2prt + str(ind)):
		os.system('gunzip -c '+ JV.p2prt + str(ind)+ '/' + '*.fastq.gz > '+ JV.p2prt + fname)
		if file_size(JV.p2prt + fname) > 0:
			nbr_reads,nbr_spikes = file_word_len(JV.p2prt + fname,'Spike')
			nbr_reads /= 4
			f.write('%10s%15d%15d%15f'%(fname,nbr_spikes,nbr_reads,float(nbr_spikes)/nbr_reads))
		else:
			f.write('%10s%15d%15d%15f'%(fname,0,0,0.0))

		f.write('\n')
		remove_file(JV.p2prt + fname)

	f.close()
	print 'stats were written into:\n' + p2file


def virtual_part_dist(JV,ind):

	path = JV.oDir + JV.sdrPART + JV.dec_cls_dir() + str(ind) + '/'
	if not os.path.isdir(path):
		return

	from fastq_reader import Fastq_Reader
	from collections import defaultdict

	HO = Fastq_Reader(JV.oDir)

	LS = get_file_list(PATH = path, PFX = 'job', END = '*gz',PFX_dir = True)
	print len(LS)
	gCounts =  defaultdict(int)
	NM = []
	for fl in LS:
		print '--------------'
		print fl
		f = open_file(fl)
		fGEN = HO.read_from_fastq(f,paired_reads=HO.paired_reads)

		for fastq_read in fGEN:
			gName = fastq_read[(fastq_read.find('_')+1):fastq_read.find('.')]
			gCounts[gName] += 1
			NM.append(gName)
		f.close()
	path = JV.oDir + JV.sdrTMP + 'virtual/' + JV.dec_cls_dir()
	save_data(gCounts,path + str(ind) + '.vrt',dump='pk')
	save_data(NM,path + str(ind) + '.nm',fl='list')

def cami_part_dist(JV,ind):

	GL = load_data(JV.oDir + JV.sdrJOB + '/' + 'genomes.list',fl='list')
	SL = load_data(JV.oDir + JV.sdrJOB + '/' + 'samples.list',fl='list')
	DCL = load_data(JV.oDir + JV.sdrJOB + '/' + 'DC.list',fl='list',dump='pk')

	path = JV.oDir + JV.sdrPART + JV.dec_cls_dir() + str(ind) + '/'
	if not os.path.isdir(path):
		return

	from fastq_reader import Fastq_Reader
	from collections import defaultdict

	HO = Fastq_Reader(JV.oDir)

	LS = get_file_list(PATH = path, PFX = 'job', END = '*gz',PFX_dir = True)
	print len(LS)
	gCounts =  defaultdict(int)
	for fl in LS:
		print '--------------'
		print fl
		f = open_file(fl)
		fGEN = HO.read_from_fastq(f,paired_reads=HO.paired_reads)

		for fastq_read in fGEN:
			rName = fastq_read[(fastq_read.find('@')+1):fastq_read.find('/')]
			sNbr = int(rName[1:rName.find('R')])
			rNbr = int(rName[rName.find('R')+1:])
			gName = DCL[SL.index(sNbr)][rNbr]
			gCounts[gName] += 1
		f.close()
	path = JV.oDir + JV.sdrTMP + 'cami/' + JV.dec_cls_dir()
	save_data(gCounts,path + str(ind) + '.cmi',dump='pk')

def get_node_fasta(p2f):

	node_ACGT = ''
	with open(p2f, 'r') as myfile:
		for line in myfile:
			
			line  = line.strip()
			if len(line)==0:
				continue
			if 'NODE' in  line:
				if len(node_ACGT) > 0:
					yield node_name,node_ACGT
					node_ACGT = ''
				node_name = line
			else:
				node_ACGT += line.upper()
		if len(node_ACGT) > 0:
			yield node_name,node_ACGT


def filter_fasta_nodes_name(path2file,JV):

	fName = get_file_name(path2file)
	dName = get_dir_name(path2file)

	dirBounds = JV.filtered_scaffolds_dir()
	new_file_name = dName + dirBounds + fName
	return new_file_name

def filter_fasta_nodes(path2file,JV):

	new_file_name = filter_fasta_nodes_name(path2file,JV)

	bnd_len = [int(JV.fasta_nodes_len_lb), int(JV.fasta_nodes_len_ub)]
	bnd_cov = [float(JV.fasta_nodes_cov_lb), float(JV.fasta_nodes_cov_ub)]

	f = open_file(new_file_name, 'w')

	for node_name,node_ACGT in get_node_fasta(path2file):
		node_name_blocks = node_name.split('_')
		#node_len = len(node_ACGT)
		node_len = int(node_name_blocks[3])
		node_cov = float(node_name_blocks[5])
		flg = True
		if (bnd_len[0] > 0) and (node_len<bnd_len[0]):
			flg = False
		if (bnd_len[1] > 0) and (node_len>bnd_len[1]):
			flg = False
		if (bnd_cov[0] > 0) and (node_cov<bnd_cov[0]):
			flg = False
		if (bnd_cov[1] > 0) and (node_cov>bnd_cov[1]):
			flg = False
		
		if flg:
			f.write(node_name +'\n')
			f.write(node_ACGT +'\n')
	f.close()
	return new_file_name


def count_hcv(CN):
	if len(CN.shape) != 2:
		raise NameError('CN must be a matrix')

	CN = CN.astype(np.float)
	nC,nK = CN.shape
	n = CN.sum()
	Nc = CN.sum(1)
	Nk = CN.sum(0)
	pc = Nc/n
	pk = Nk/n
	HC = 0.0
	HCK = 0.0
	for i in range(nC):
		if pc[i]:
			HC -= (pc[i]*np.log(pc[i]))
		for j in range(nK):
			if CN[i,j]:
				HCK -= (CN[i,j]/n)*np.log(CN[i,j]/Nk[j])

	h = 1.0
	if HC:
		h -= HCK/HC

	HK = 0.0
	HKC = 0.0
	for j in range(nK):
		if pk[j]:
			HK -= (pk[j]*np.log(pk[j]))
		for i in range(nC):
			if CN[i,j]:
				HKC -= (CN[i,j]/n)*np.log(CN[i,j]/Nc[i])

	c = 1.0
	if HK:
		c -= HKC/HK
	v = (2 * h * c) / (h + c)
	return h,c,v
