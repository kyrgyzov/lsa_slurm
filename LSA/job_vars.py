import sys,os
from lsa_job import *
from lsa_names import *
import shutil
def array_size_step(AS,MAS):

	step = AS / MAS
	if AS % MAS != 0:
		step += 1

	if AS <= MAS:
		SAS = AS
	else:
		SAS = MAS
	
	return SAS,step


class JobVars():
    
    def __init__(self,p2file = ''):

        if p2file:
            fl = 'prj'
            if fl[-1] is '/':
                fl = 'job'
                    
            pV = load_data(p2file, fl = fl)
            for key,value in pV.items():
                setattr(self,key,value)
            del pV
	    self.p2prt = self.oDir + self.sdrPART + self.dec_cls_dir()

    def load(self,p2file):
        fl = 'prj'
        if fl[-1] is '/':
            fl = 'job'
                    
        pV = load_data(p2file, fl = fl)
        for key,value in pV.items():
            setattr(self,key,value)
        del pV

    def save(self):
       	save_data(self.__dict__,self.oDir,fl='job')
    
    def share(self,OBJ,sList):
	    for fld in sList:
		    setattr(OBJ,fld,str2val(getattr(self,fld)))

    def add(self,key,value):
        setattr(self,key,value)

    def get(self,key):
        getattr(self,key)

    def delete(self,key):
        delattr(self,key)

    def pr(self):
        dc = self.__dict__
        for key in sorted(dc):
            print key,',',dc[key]

    def make_prj_space(self):
        self.oDir += ('LSA_' + self.hName)

	if self.hName == 'hyper':
		self.oDir += ('_k' + self.kMer + '_h' + self.hSize)
	elif self.hName == 'gallager':
		self.oDir += ('_k' + self.kMer + '_m' + self.mLDH + '_t' + self.tLDH)
	#else:
	#	raise NameError('Hash name is not defined')

	
	self.oDir +=('_qc'+self.check_qc[0] + '_fu'+self.filter_uq[0] + '/')


        if (self.jobID == 'SetupProject') or (self.jobID == 'Master'):
		self.get_prj_info()
		for key,item in self.__dict__.items():
			if (key[:3] == 'sdr') and (item[-1] == '/'):
				os.system('mkdir -p ' + self.oDir + item)

    def remove_old_slurm_files(self):

	    sname,oname,ename = self.get_slurm_job_file_name()
	    remove_file(sname)
            print sname,'file was removed'
	    
	    o_dir = get_dir_name(oname)
	    e_dir = get_dir_name(ename)
	    remove_dir(oname[:oname.rfind('out/')])
	    os.system('mkdir -p ' + o_dir)
	    os.system('mkdir -p ' + e_dir)
	    

    def make_slurm_job(self):
	    os.system('python ' + self.cDir + 'SLURM/create_lsa_jobs.py -p ' + self.prjID + ' -j ' + self.jobID)
	    sname,oname,ename = self.get_slurm_job_file_name()
	    return sname,oname,ename

    def convert_slurm_job(self,p2job,p2out,p2err,prt,pref=None):
	    sName = str_rep_dict(p2job,{pref+'ID':str(prt)})
	    oName = str_rep_dict(p2out,{pref+'ID':str(prt)})
	    eName = str_rep_dict(p2err,{pref+'ID':str(prt)})

	    remove_file(sName)
	    remove_dir(oName[:oName.find('out/')])

	    os.rename(p2job,sName)
	    shutil.move(p2out[:p2out.rfind('out/')],oName[:oName.find('out/')])


	    file_text = load_data(sName,fl='str')
	    file_text = rep_in_str(file_text,{pref+'ID':str(prt)})
	    save_data(file_text,sName,fl='str')
	    return sName,oName,eName
    
    def get_prj_info(self):
      
	try:
		CPU = os.popen('scontrol show partition ' + self.sPart + ' | grep TotalCPUs').read().split()[1]
		CPU = int(CPU[CPU.index('=')+1:])
	except:
		CPU = 1
	try:
		MAS = os.popen('scontrol show config | grep MaxArraySize').read()
		MAS = int(MAS[MAS.index('=')+2:])
	except:
		MAS = 1

	self.add('MAS',str(min(MAS,CPU)))
      	self.add('memSlurm',str(int(self.memSpades)*1024))

    def get_slurm_job_file_name(self):

        jname = self.oDir + self.sdrSCR + self.jobID + '.slurm.job'
        oname = self.oDir + self.sdrLOG + self.jobID + '/out/' + self.jobID + '_%a.slurm.out'
        ename = self.oDir + self.sdrLOG + self.jobID + '/err/' + self.jobID + '_%a.slurm.err'

        return jname,oname,ename

    def get_files_info(self):
	    print 'get_files_info'
	    lFiles = get_file_list(self.iDir,self.PFX,'','*.fastq*',PFX_dir = True)
	    lFiles.sort()

	    sNames = get_file_name(lFiles)
	    sNames = get_prefix_from_name(sNames)
	    sNames = list(set(sNames))
	    sNames.sort()
      
	    save_data(sNames,self.oDir + self.sdrJOB + 'sampleList.txt',fl='str')
	    save_data(lFiles,self.oDir + self.sdrJOB + 'fileList.txt',fl='str')
	    print 'saved'
	    SZ = [ int(os.path.getsize(f)) for f in lFiles ]
	    si = sorted(range(len(SZ)),key=lambda j: SZ[j])
	    mi = si[len(si)/2]

	    r = nbr_lines_file(lFiles[mi])
	    self.add('fReads', str(r/4))
	    print 'nbr_lines_file',r

	    self.add('nbr_S',str(len(sNames)))
	    self.add('nbr_F',str(len(lFiles)))

	    f = open_file(lFiles[mi])
	    PR = paired_reads(f)
	    print 'PR',PR
	    QC = get_quality_code(f)
	    print 'QC',QC
	    RL = read_len(f)
	    print 'RL',RL
	    f.close()
	    self.add('paired_reads', str(PR))
	    self.add('QC', str(QC))
	    self.add('nbr_NR', str(RL))

	    self.save()

    def get_fa_files_info(self):

        lFiles = get_file_list(self.iDir,self.PFX,'','*.fa*',PFX_dir = True)
        lFiles.sort()

        sNames = get_file_name(lFiles)
        sNames = get_prefix_from_name(sNames)
        sNames = list(set(sNames))
        sNames.sort()
      
	save_data(sNames,self.oDir + self.sdrJOB + 'sampleList.txt',fl='str')
	save_data(lFiles,self.oDir + self.sdrJOB + 'fileList.txt',fl='str')

        #self.add('fReads', str(1000))

        self.add('nbr_S',str(len(sNames)))
        self.add('nbr_F',str(len(lFiles)))

        self.add('paired_reads', str(True))
        self.add('QC', str(33))
        self.add('nbr_NR', str(400))

        self.save()

    def get_nbr_elements(self,param):

	    if ('files' in param):
		    AS = int(self.nbr_F)
	    elif ('max' in param):
		    AS = int(self.MAS)
	    elif ('samples' in param):
		    AS = int(self.nbr_S)
	    elif ('chunks' in param):
		    fname = self.oDir + self.sdrCLS + 'nbrHashes.txt'
		    nbr_H = load_data(fname,fl='nbr')
		    cMax = 2**int(self.cSize)
		    nbr_C = nbr_H / cMax
		    if nbr_H % cMax != 0:
			    nbr_C += 1
		    AS = nbr_C
	    elif ('clusters' in param):
		    AS = load_data(self.oDir + self.sdrCLS + self.dec_cls_dir() +  'nbrClusters.txt',fl='nbr')
	    elif ('vectors' in param):
		    fname = self.path2decomp_file()
		    AS = len(load_data(fname + '.s.npy',fl='npy'))
	    else:
		    raise NameError('no name in param array')
	    return AS

    def get_array_size(self,param):

	    MAS = int(self.MAS)
	    AS = self.get_nbr_elements(param)
	    SAS,step = array_size_step(AS, MAS)
	    return SAS,step
    
    def hash_weight_type(self):
	    return self.hwType

    def hash_weight_file(self):
	    return self.oDir + self.sdrCLS + 'hash_weights_' + self.hwType

    def cond_file(self):
	    return self.hwType + '_' + self.rnType + '_' + self.cnType + '.cond'

    def decomp_file(self):
	    return self.hwType + '_' + self.rnType + '_' + self.cnType + '_' + self.decType + '_' + self.decRank

    def cls_file(self):
	    ps = self.clsType
	    if self.clsType == 'CosineMeans':
		    ps += ('_thr_' + self.clThresh) 
	    return ps

    def path2decomp_file(self):
	    return self.oDir + self.sdrCLS + self.decomp_file()

    def path2hash_base_file(self):
	    return self.oDir + self.sdrJOB + self.hName

    def dec_cls_dir(self):
	    return self.decomp_file() + '_' + self.cls_file() + '/'

    def hash_max(self):
	    if self.hName == 'hyper':
		    hMax = 2**int(self.hSize)
	    elif self.hName == 'gallager':
		    hMax = 5**int(self.tLDH)
	    else:
		    raise NameError('Hash name is not defined')
	    return hMax
    def filtered_scaffolds_dir(self):
	    
	    p2res = 'scaffolds_len_' + self.fasta_nodes_len_lb + '_' + self.fasta_nodes_len_ub + '_cov_'+ self.fasta_nodes_cov_lb + '_' + self.fasta_nodes_cov_ub + '/'
	    return p2res

    def report_spades_dir(self):
	    
	    p2res = self.oDir + self.sdrREP + 'spades/' + self.dec_cls_dir()  + '/spades_prt_' + self.prtID + '/'
	    return p2res
