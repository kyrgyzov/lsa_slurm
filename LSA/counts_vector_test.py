import time
import random
from counts_vector import Counts_Vector
import numpy as np

sz=2**30
H=np.zeros(sz,dtype=np.uint)
a=Counts_Vector()
for i in xrange(1000):
    print '--------------'
    v=random.randint(0,sz-1)
    print i,v
    H[v]+=1
    a.inc(v)

Lh=list(np.nonzero(H)[0])
La=list(a.I)
len(Lh),len(La),len(list(set(Lh)&set(La)))
sum([Lh[i]==La[i] for i in range(len(La))])
Vh=H[Lh]
Va=a.V
sum([Vh[i]==Va[i] for i in range(len(La))])

print 'test time'
nbr_step = 10**5
t1=time.time()
for i in xrange(nbr_step):
    v=random.randint(0,sz-1)
    H[v]+=1
print time.time()-t1

t1=time.time()
for i in xrange(nbr_step):
    v=random.randint(0,sz-1)
    a.inc(v)
print time.time()-t1
