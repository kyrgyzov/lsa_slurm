#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from lsa_stats import count_hcv
from job_vars import JobVars
import yaml
from collections import defaultdict


help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	nK = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir() + '/nbrClusters.txt',fl='nbr') 

	JV.jobID = 'CamiPrt'
	jname,oname,ename = JV.make_slurm_job()

	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,int(JV.wts),job = jname)

	
	GL = load_data(JV.oDir + JV.sdrJOB + '/' + 'genomes.list',fl='list')

	nC = len(GL)
	CN = np.zeros((nC,nK),dtype=np.int)

	ST = map(str,GL[:])
	for j in range(nK):
		fName = JV.oDir + JV.sdrTMP + 'cami/' + JV.dec_cls_dir() + str(j)+'.cmi'
		if not os.path.isfile(fName):
			continue

		tD = load_data(fName,dump='pk')
		for i,nm in enumerate(GL):
			val = tD[nm]
			ST[i] += (',' + str(val))
			CN[i,j] = val

	save_data(ST,JV.oDir + JV.sdrREP + 'cami/' + JV.dec_cls_dir() + 'cami_table.txt',fl='str')

	h,c,v = count_hcv(CN)

	ST = ['Homogeneity, ' + str(h), 'Completeness, ' + str(c), 'V-measure, ' + str(v)]
	save_data(ST,JV.oDir + JV.sdrREP + 'cami/' + JV.dec_cls_dir() + 'cami_hcv.txt',fl='str')

	ST=['#prt,#gns,entrp']
	STI=[]

	for j in range(nK):
		nzi = np.nonzero(CN[:,j])[0]
		vec = CN[nzi,j].astype(np.float)
		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0
		rc =[j,nbs,entrp]
		ST.append(','.join(map(str,rc)))
		rc =np.append(j,nzi)
		STI.append(','.join(map(str,rc)))

	save_data(ST,JV.oDir + JV.sdrREP + 'cami/' + JV.dec_cls_dir() + 'cami_parts.txt',fl='str')
	save_data(STI,JV.oDir + JV.sdrREP + 'cami/' + JV.dec_cls_dir() + 'cami_parts_genomes_ind.txt',fl='str')

	ST=['#col,#gName,#parts,entrp']
	STI=[]
	for i in range(nC):
		nzi = np.nonzero(CN[i,:])[0]
		vec = CN[i,nzi].astype(np.float)
		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0
		rc =[i,GL[i],nbs,entrp]
		ST.append(','.join(map(str,rc)))
		rc =np.append(i,nzi)
		STI.append(','.join(map(str,rc)))

	save_data(ST,JV.oDir + JV.sdrREP + 'cami/' + JV.dec_cls_dir() + 'cami_genomes.txt',fl='str')
	save_data(STI,JV.oDir + JV.sdrREP + 'cami/'+ JV.dec_cls_dir() + 'cami_genomes_parts_ind.txt',fl='str')

