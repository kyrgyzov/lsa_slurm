import numpy as np
from scipy.spatial import distance
from numpy import linalg as la
from numpy import sqrt
from collections import defaultdict
from multiprocessing import Pool
from operator import itemgetter
from lsa_base import LSA
from hyper_sequences import Hyper_Sequences
from hash_counting import Hash_Counting
import glob,os,random,time,sys
from lsa_job import *
import  gc
import marshal as mr
import scipy.sparse as spr
import scipy.optimize as sop
from math import floor,sqrt,log
from counts_vector import Counts_Vector
from job_vars import JobVars
from lsa_stats import SafeRt

class StreamingEigenhashes(Hash_Counting,Hyper_Sequences,LSA):

	def __init__(self,oDir):
		super(StreamingEigenhashes,self).__init__(oDir)

		JV = JobVars(oDir)
		self.hMax = JV.hash_max()
		self.decomp_file = JV.path2decomp_file()
		self.cond_file = JV.cond_file()
		self.hash_weight_file = JV.hash_weight_file()
		self.clone(JV)
		self.init_lsa()

		self.rn_p = 2
		if self.rnType[:2] == 'nr':
			self.rn_p = int(self.rnType[2:])

		self.cn_p = 2
		if self.cnType[:2] == 'nr':
			self.cn_p = int(self.cnType[2:])
		del JV

	def szIndMat(self):
		return (2*self.nbr_T,self.nbr_H)

	def ms_m(self):
		return 1+int(float(self.mS_m_cf)*np.sqrt(self.nbr_H))

	def get_hash_indices(self):

		H = Counts_Vector()
		print 'hash_max - ',self.hMax
		for i in range(self.nbr_S):
			nzi = load_data(self.sampleHashFile[i] + '.nzi',fl='list')
			hv = load_data(self.sampleHashFile[i],fl='mm',dump=np.uint16)
			arr = [int(v) for v in hv]
			H.add_IV(nzi,arr)
			
			rt = SafeRt(len(H.I),self.hMax)
			print i,self.nbr_S,'% of the nonzeros to hash_max - ',rt
			del nzi
			
		return H.I,H.V

	def get_hash_weights(self):

		H = np.zeros(self.nbr_H,dtype=np.float32)

		print 'hash_max - ',self.hMax
		for i in range(self.nbr_S):
			nzi = load_data(self.sampleHashFile[i] + '.nzi',fl='mm',dump=np.uint64)
			H[nzi] += 1
			
			rt = SafeRt(np.sum(H!=0),self.hMax)
			print '% of the nonzeros to hash_max - ',rt
			del nzi

		if self.hwType == 'IDF':
			HW = np.log2((1.0 + self.nbr_S)/H)/np.log2(1 + self.nbr_S)

		elif self.hwType == 'Entropy': 
			S = np.zeros(self.nbr_H,dtype=np.float32)
			E = np.zeros(self.nbr_H,dtype=np.float32)

			for i in range(self.nbr_S):
				nzi = load_data(self.sampleHashFile[i] + '.nzi',fl='mm',dump=np.uint64)
				hv = load_data(self.sampleHashFile[i],fl='mm',dump=np.uint16)
				S[nzi] += hv
				E[nzi] += (hv*np.log2(hv))
				del hv
			nr = np.log2(1 + self.nbr_S)
			HW = 1 - np.log2(S)/nr + E/(S*nr)

		elif self.hwType == 'Equal':
			HW = np.ones(H.size,dtype = np.float32)
		else:
			raise NameError(hwType + 'is not known')

		return HW

	def get_hash_row_stats(self):
		MN = np.zeros(self.nbr_S,dtype=np.float32)
		SD = np.zeros(self.nbr_S,dtype=np.float32)
		NR = np.zeros(self.nbr_S,dtype=np.float32)
		SZ = np.zeros(self.nbr_S,dtype=np.float32)
		for i in range(self.nbr_S):
			H = load_data(self.sampleHashFile[i],fl='mm',dump=np.uint16)
			H = np.float32(H)
			MN[i] = np.mean(H)
			SD[i] = np.std(H)
			NR[i] = la.norm(H,ord=self.rn_p)
			SZ[i] = H.size
			
		return MN,SD,NR,SZ
		
	def sample_hash_counts_from_disc(self):

		class NewCorpus(object):
			def __iter__(newself):
				for i in range(self.nbr_S):
					yield (self.IFL[i],self.CFL[i])
		return NewCorpus()

	def get_position_in_list(self,I,v):
		ln=len(I)
		a = 0
		b = ln-1
		if ln == 0:
			return a,0
		if v <= I[a]:
			return a,ln
		if I[b] < v:
			return ln,ln
		if I[b] == v:
			return b,ln
		c=(a+b)/2
		while True:		    
			if I[c]==v:
				return c,ln
			if v<I[c]:
				b=c
			else:
				a=c
			if (b-a) < 2:
				return b,ln
			c=(a+b)/2



	def get_start_index(self,I,v):
		nbr=len(I)
		IND = np.zeros(nbr,dtype=np.uint64)
		LN = np.zeros(nbr,dtype=np.uint64)
		for i in range(nbr):
			LN[i]=len(I[i])
			for j in range(LN[i]):
				if I[i][j]>=v:
					IND[i]=j
					break
		return IND,LN

	def get_start_index_fast(self,I,v):
		nbr=len(I)
		IND = np.zeros(nbr,dtype=np.uint64)
		LN = np.zeros(nbr,dtype=np.uint64)
		for i in range(nbr):
			ps,ln = self.get_position_in_list(I[i],v)
			IND[i] = ps
			LN[i]  = ln
		return IND,LN

	def open_AM_files(self):
		if self.fullCM:
			self.CM = np.memmap(self.oDir + self.sdrCLS + self.cond_file, dtype = np.float32, mode='r', shape=(self.nbr_S,self.nbr_H))
		else:
			self.open_hash_files()

	def close_AM_files(self):
		if self.fullCM:
			del self.CM
		else:
			self.close_hash_files()		

	def open_hash_files(self):
		import resource
		soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
		nbrf = 3*self.nbr_S
		if soft < nbrf:
			resource.setrlimit(resource.RLIMIT_NOFILE, (nbrf, hard))
		if not hasattr(self,'CFL'):
			self.CFL = [load_data(self.sampleHashFile[i],fl='mm',dump=np.uint16) for i in range(self.nbr_S)]
			self.IFL = [load_data(self.sampleHashFile[i]+'.nzi', fl='mm',dump=np.uint64)  for i in range(self.nbr_S)]

	def open_sps(self):
		spsFile = self.oDir + self.sdrCLS + str(self.cSize) + '.sps'
		if not os.path.isfile(spsFile):
			self.make_sps()
		self.SPS = load_data(spsFile, fl='mm',dump=np.uint64)
		self.SPS.shape = (self.nbr_S,-1)


	def open_am_stat_vecs(self):

		self.hash_weight = load_data(self.hash_weight_file, fl='mm',dump=np.float32)
		self.hash_row_size = load_data(self.oDir +  self.sdrCLS + 'hash_row_size', fl='mm',dump=np.float32)
		self.hash_row_norm = load_data(self.oDir +  self.sdrCLS + 'hash_row_norm'+ str(self.rn_p), fl='mm',dump=np.float32)
		self.hash_row_mean = load_data(self.oDir +  self.sdrCLS + 'hash_row_mean', fl='mm',dump=np.float32)
		self.hash_row_std =  load_data(self.oDir +  self.sdrCLS + 'hash_row_std', fl='mm',dump=np.float32)

	def make_sps(self):

		spsFile = self.oDir + self.sdrCLS + str(self.cSize) + '.sps'
		if os.path.isfile(spsFile):
			return

		import resource
		soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
		nbrf = 3*self.nbr_S
		if soft < nbrf:
			resource.setrlimit(resource.RLIMIT_NOFILE, (nbrf, hard))

		print time.ctime(),'start sps'

		SPS = np.zeros((self.nbr_S,self.nbr_C+1),dtype = np.uint64)
		self.II = [load_data(self.sampleHashFile[i]+'.nzi', fl='mm',dump=np.uint64)  for i in range(self.nbr_S)]

		for i in range(self.nbr_C):
			print i
			strC = i*self.cMax
			IND,LN = self.get_start_index_fast(self.II,strC)
			SPS[:,i] = IND

		SPS[:,self.nbr_C] = LN

		self.II = []
		gc.collect()
		print 'SPS shape -',SPS.shape
		save_data(SPS,spsFile,fl='mm')
		print time.ctime(),'end sps'


	def close_hash_files(self):		
		for f in self.CFL:
			del f
		del self.CFL
		for f in self.IFL:
			del f
		del self.IFL
		gc.collect()

	def close_sps(self):		
		del self.SPS
		gc.collect()

	def close_am_stat_vecs(self):
		del self.hash_weight
		del self.hash_row_size
		del self.hash_row_norm
		del self.hash_row_mean
		del self.hash_row_std

		gc.collect()

	def normalize_hash_row(self,row,i):

		if self.rnType == 'no':
			pass
		elif self.rnType[:2] == 'nr':

			row *= self.nbr_H
			row /= self.hash_row_norm[i]
			#row *= np.sqrt(self.hash_row_size[i])
		elif self.rnType == 'st':
			row -= self.hash_row_mean[i]
			row /= self.hash_row_std[i]
		else:
			raise NameError(self.rnType + ' is not known')
		return row

	def normalize_hash_col(self,col,j):

		if self.cnType == 'no':
			pass
		elif self.cnType[:2] == 'nr':
			col *= self.nbr_T
			col /= la.norm(col,ord=self.cn_p)
		elif self.cnType == 'st':
			col -= np.mean(col)
			col /= np.std(col)
		elif self.cnType == 'hw':
			col *= self.hash_weight[j]
		else:
			raise NameError(self.cnType + ' is not known')
		return col

	def normalize_hash_mat(self,M,strC,szC):

		if self.cnType == 'no':
			pass
		elif self.cnType[:2] == 'nr':
			M /= la.norm(M,axis=0,ord=self.cn_p)
		elif self.cnType == 'st':
			M -= np.mean(M,axis=0)
			M /= np.std(M,axis=0)
		elif self.cnType == 'hw':
			M *= self.hash_weight[strC:strC+szC]
		else:
			raise NameError(self.cnType + ' is not known')
		return M

	def make_full_cond(self):
		p2file = self.oDir + self.sdrCLS + self.cond_file

		if os.path.isfile(p2file):
			return
		self.open_am_stat_vecs()

		M = np.memmap(p2file, dtype = np.float32, mode='w+', shape=(self.nbr_S,self.nbr_H))
		strC = 0
		szC = self.nbr_H

		for i in range(self.nbr_S):
			print 'i',i,self.nbr_S
			CFL = load_data(self.sampleHashFile[i],fl='mm',dump=np.uint16)
			IND = load_data(self.sampleHashFile[i]+'.nzi', fl='mm',dump=np.uint64)
			line=np.zeros(CFL.size,np.float32)
			line[:] = CFL[:]
					
			line = self.normalize_hash_row(line,i)
			M[i,IND] = line
		M = self.normalize_hash_mat(M,strC,szC)
		del M
		self.close_am_stat_vecs()
		print 'full cond is done'

	def get_full_hash_chunk(self,ind):
		
		strC = ind*self.cMax
		szC = min(self.cMax,self.nbr_H-strC)
		M = np.zeros((self.nbr_S,szC),dtype=np.float32)

		if self.fullCM:
			strC = ind*self.cMax
			szC = min(self.cMax,self.nbr_H-strC)
	        	M[:] = self.CM[:,strC:strC+szC]
		else:
			for i in range(self.nbr_S):
				#print 'i',i
				st = self.SPS[i,ind]
				sp = self.SPS[i,ind+1]
				#print i,st,sp,self.IFL[i].size,self.CFL[i].size
				line = np.copy(self.CFL[i][st:sp]).astype(np.float32)
				IND = self.IFL[i][st:sp]
				line = self.normalize_hash_row(line,i)
				M[i,IND-strC] = line
			M = self.normalize_hash_mat(M,strC,szC)

		return M

	def get_gen_hash_chunk(self,ind):

		M = self.get_full_hash_chunk(ind)

		class NewCorpus(object):
			def __iter__(newself):
				
				for c in xrange(M.shape[1]):
					yield M[:,c]

		return NewCorpus()

	def get_gen_hash(self,full=True):
		print 'total # of chunks: ',self.nbr_C
		class NewCorpus(object):
			def __iter__(newself):
				for ind in xrange(self.nbr_C):
					print 'chunk - ',ind
					gen = self.get_gen_hash_chunk(ind)
					for el in gen:
						if full:
							yield el
						else:
							yield self.vec_full2sparse(el)
					del gen
		return NewCorpus()

	def transform_vectors(self,model,hash_gen):
		if self.decType == 'none':
			seed_vectors = hash_gen
		elif 'gensim' in self.decType:
			seed_vectors = self.gensim_transform(model,hash_gen)
		elif 'sklearn' in self.decType:
			seed_vectors = self.sklearn_transform(model,hash_gen)
		elif 'sparsek' in self.decType:
			seed_vectors = self.sparsek_transform(model,hash_gen)
		elif 'spams' in self.decType:
			seed_vectors = self.spams_transform(model,hash_gen)
		elif 'power' in self.decType:
			seed_vectors = self.power_transform(model,hash_gen)
		else:
			raise NameError('unknown name of model ' + self.decType)

		return seed_vectors

	def load_clusters(self):

		save_dir = self.oDir + self.sdrCLS + self.dec_cls_dir
		self.nbr_Cls = load_data(save_dir + 'nbrClusters.txt',fl='nbr')

		if self.clsType == 'CosineMeans':
			clusters = load_data(save_dir + 'cluster_centroids.npy',fl='npy')

		elif self.clsType == 'IndMax':
			clusters = []
		elif self.clsType == 'Nzi':
			clusters = []
		elif self.clsType == 'mSpace':
			clusters = []
		else:
			raise NameError('unknown name of model ' + self.decType)
		return clusters
		
	def load_decomp_model(self):
		print 'decomp_model -',self.decType
		if self.decType == 'none':
			self.nbr_T = self.nbr_S
			model = None
		elif 'gensim' in self.decType:
			print 'looking at gensim'
			from gensim import models
			model = models.LsiModel.load(self.decomp_file)
			print model
			print model.num_topics
			self.nbr_T = model.num_topics
		elif 'sklearn' in self.decType:
			if self.decType == 'sklearn.svd':
				from sklearn.decomposition import TruncatedSVD
				model = load_data(self.decomp_file, dump ='pk')
			elif self.decType == 'sklearn.nmf':
				from sklearn.decomposition import NMF
				model = load_data(self.decomp_file, dump ='pk')
				model.set_params(verbose=False)
			else:
				raise NameError('unknown name of model ' + self.decType)

			self.nbr_T = model.components_.shape[0]

		elif 'spams' in self.decType:
			model = load_data(self.decomp_file+'.U.npy', fl='npy')
			self.nbr_T = model.shape[1]

		elif 'power' in self.decType:
			s  = load_data(self.decomp_file + '.s.npy',fl='npy')
			model  = load_data(self.decomp_file + '.u.npy',fl='npy')
			self.nbr_T = len(s)
		elif 'sparsek' in self.decType:
			model  = load_data(self.decomp_file + '.u.npy',fl='npy')
			self.nbr_T = model.shape[1]
		else:
			raise NameError('unknown name of model ' + self.decType)
		return model

	def matrix_rows2vec_gen(self,M):

		class NewCorpus(object):
			def __iter__(newself):
				for r in xrange(M.shape[0]):
					R=[]
					for c in range(M.shape[1]):
						if M[r,c] != 0:
							R.append((c,float(M[r,c])))
					yield R
				gc.collect()


		return NewCorpus()

	def gensim_transform(self,model,vec_gen):

		class NewCorpus(object):
			def __iter__(newself):
				for doc in vec_gen:
					vec = self.vec_full2sparse(doc)
					tr = model[vec]
					res = self.vec_sparse2full(tr,self.nbr_T)
					yield res
				gc.collect()

		return NewCorpus()

	def spams_transform(self,model,vec_gen):

		model = np.asfortranarray(model,dtype=np.float32)
		pos = 'nmf' in self.decType
		pos = False
		if 'nmf' in self.decType:
			pos = True 

		class NewCorpus(object):
			def __iter__(newself):
				import spams

				for doc in vec_gen:
					v = np.copy(doc)
					v.shape=(self.nbr_S,1)
					v=np.asfortranarray(v)
					tr = spams.lasso(v,D=model,lambda1=0.1,lambda2=0.05,pos=pos,mode=2)
					tr = tr.toarray()
					tr.shape=(self.nbr_T,)

					yield tr

				gc.collect()

		return NewCorpus()

	def sklearn_transform(self,model,vec_gen):

		class NewCorpus(object):
			def __iter__(newself):
				for doc in vec_gen:
					doc.shape=(1,self.nbr_S)
					tr = model.transform(doc)[0]
					yield tr

				gc.collect()

		return NewCorpus()

	def sparsek_transform(self,model,vec_gen):

		class NewCorpus(object):
			def __iter__(newself):
				for doc in vec_gen:
					tr = sop.nnls(model,doc)[0]
					yield tr

				gc.collect()

		return NewCorpus()

	def power_transform(self,model,vec_gen):

		class NewCorpus(object):
			def __iter__(newself):
				for vec in vec_gen:
					R = vec.dot(model)
					yield R
		return NewCorpus()

	def vec_full2sparse(self,vec):
		R=[]
		for i in range(len(vec)):
			if vec[i]:
				R.append((i,float(vec[i])))
		return R
			

	def vec_sparse2full(self,ls,sz):
		vec = np.zeros(sz,dtype=np.float32)
		for el in ls:
			vec[el[0]] = el[1]
		return vec

	def vec2block(self,vectors):
		block = []
		for doc in vectors:
			a = np.zeros(self.nbr_T,dtype=np.float32)
			for x in doc:
				a[x[0]] = x[1]
			block.append(a)
		return block

	def vec2mat(self,vectors):
		block = []
		for doc in vectors:
			block.append(doc)
		nv = len(block)
		M = np.zeros((self.nbr_T,nv),dtype=np.float32)

		for i in xrange(nv):
			for x in block[i]:
				M[x[0],i] = x[1]
		return M

	def model_cluster_chunk(self,ind,model,Centroids,GW):
		print '---------------------------------------------'

		Hash_Cluster = []
		CC = [0]
		
		CP = np.zeros(self.nbr_Cls)
		
		hash_gen = self.get_gen_hash_chunk(ind)
		all_vectors = self.transform_vectors(model,hash_gen)
		st = ind * self.cMax
                if self.clsType == 'mSpace':
			from space_m import uv_un_D

			read_dir = self.oDir + self.sdrTMP + self.dec_cls_dir
			VI = load_data(read_dir+'VI', fl='mm',dump = np.uint64)
		        uv,un,D = uv_un_D(VI)


		for col,doc in enumerate(all_vectors):
			if self.clsType == 'CosineMeans':
				fits = distance.cdist([doc],Centroids,'cosine')[0]

				clust = fits.argmin()
				res = [self.nbr_Cls-1]
				if fits[clust] < self.clThresh:
					res = [clust]

			elif self.clsType == 'IndMax':
				res = [np.argmax(np.absolute(doc))]

			elif self.clsType == 'Nzi':
				res = np.nonzero(doc)[0]
			elif self.clsType == 'mSpace':
				res = [D[VI[col+st]]]
			else:
				raise NameError('Unknown clsType - ' + self.clsType)

			CC.append(CC[-1] + len(res))
			wt = GW[col+st]
			for v in res:
				Hash_Cluster.append(int(v))
				#CS[v] += 1
				CP[v] +=  wt

		return Hash_Cluster,CC,CP

	def merge_index(self,V,I,C):

		random_chunk=0.00002
		chunk_size = random_chunk*self.nbr_H
		chunk_base = np.random.randint(0,2**self.cSize-chunk_size-1)		

		thresh = self.clThresh
		MergeFits = defaultdict(list)
		i = 0 
		for doc in V:
			i += 1
			if i < chunk_base:
				continue
			if i > chunk_base + chunk_size:
				break

			if I.shape[0] > 0:
				fits = distance.cdist([doc],I,'cosine')[0]
				clust = fits.argsort()[0]
				if fits[clust] < thresh:
					MergeFits[clust].append(doc)
				else:
					I = np.concatenate((I,[doc]))
					C[len(C)] = 1
			else:
				I = np.array([doc])
				C[0] = 1
		for k,v in MergeFits.items():
			I[k,:] = np.concatenate(([I[k,:]*C[k]],v)).sum(0)/(len(v)+C[k])
			C[k] += len(v)
		return C,I

	def collapse_index(self,I,C):

		combine_thresh = self.clThresh
		remove_clusters = {}
		D = distance.pdist(I,'cosine')
		D = D < (combine_thresh)
		i = 0
		j = 1
		for d in D:
			if j >= I.shape[0]:
				i += 1
				j = i+1
			if d:
				if C[i] >= C[j]:
					remove_clusters[j] = True
				else:
					remove_clusters[i] = True
			j += 1
		Cnew = {}
		for i in range(len(C)):
			if i not in remove_clusters:
				Cnew[len(Cnew)] = C[i]
		return Cnew,I[[i for i in range(len(C)) if i not in remove_clusters],:]

	def lsi_cluster_index(self,model,cluster_iters=200):
		Clusters = {}
		Index = np.zeros((0,self.nbr_T))
		for ci in range(cluster_iters):
			ind = random.randint(0,self.nbr_C-1)
			hash_gen = self.get_gen_hash_chunk(ind)
			seed_vectors = self.transform_vectors(model,hash_gen)
		
			Clusters,Index = self.merge_index(seed_vectors,Index,Clusters)
			Clusters,Index = self.collapse_index(Index,Clusters)
			print ci,len(Clusters)
		return Index
			
	def adjust_cluster_centroids(self,model,Centroids,Counts,ind):

		hash_gen = self.get_gen_hash_chunk(ind)
		seed_vectors = self.transform_vectors(model,hash_gen)

		Centroids,Counts = self.merge_vectors(Centroids,Counts,seed_vectors,np.ones(self.cMax,dtype=np.uint64))

		flg = Counts > 1
		Centroids = Centroids[flg,:]
		Counts = Counts[flg]

		sr = np.argsort(Counts)[::-1]
		Centroids = Centroids[sr,:]
		Counts = Counts[sr]
		
		return Centroids,Counts

	def merge_vectors(self,M,C,V,W):

		F = np.zeros(C.size,dtype=np.bool)

		for i in range(C.size):
			M[i,:] *= C[i]
		
		i = 0
		for doc in V:
			doc *= W[i]

			if C.size == 0:
				M = np.append(M,[doc],axis = 0)
				C = np.append(C,W[i])
				F = np.append(F,False)
				continue

			fits = distance.cdist([doc],M,'cosine')[0]
			clust = fits.argmin()

			if fits[clust] < self.clThresh:
				M[clust,:] +=  doc
				C[clust] += W[i]
				F[clust] = True
			else:
				M = np.append(M,[doc],axis = 0)
				C = np.append(C,W[i])
				F = np.append(F,False)

			i += 1

		####################################
		i = 0
		while (i < C.size) and (C.size >1):
			if not F[i]:
				i += 1
				continue

			fits = distance.cdist([M[i,:]],M,'cosine')[0]
			nzi = np.nonzero(fits < self.clThresh)[0]
			if nzi.size == 1:
				i += 1
				continue
			
			nc = np.sum(C[nzi])
			nv = np.sum(M[nzi,:],0)

			M = np.delete(M,nzi,axis = 0)
			C = np.delete(C,nzi)
			F = np.delete(F,nzi)
			
			M = np.append(M,[nv],axis = 0)
			C = np.append(C,np.uint64(nc))
			F = np.append(F,True)

		for i in range(C.size):
			M[i,:] /= la.norm(M[i,:])

		return M,C


	def tr_lin2sub(self,k,n):
		i = n - 2 - floor(sqrt(-8*k + 4*n*(n-1)-7)/2.0 - 0.5)
		j = k + i + 1 - n*(n-1)/2 + (n-i)*((n-i)-1)/2
		return int(i),int(j)

	def collapse_centroids(self,iM,iC):
		MergeV = defaultdict(list)
		MergeC = defaultdict(list)
		M = np.zeros((0,self.nbr_T))
		C = np.zeros(0,dtype=np.uint64)

		for i in xrange(iC.size):
			if C.size == 0:
				M = np.copy(iM[i,:])
				M.shape = (1,-1)
				C = np.array(iC[i],dtype=np.uint64)
			else:
				fits = distance.cdist([iM[i,:]],M,'cosine')[0]
				clust = fits.argmin()

				if fits[clust] < self.clThresh:
					MergeV[clust].append(iM[i,:])
					MergeC[clust].append(iC[i])
				else:
					M = np.append(M,[iM[i,:]],axis = 0)
					C = np.append(C,iC[i])
		for k in xrange(C.size):
			MergeV[k].append(M[k,:])
			MergeC[k].append(C[k])

		for k,v in MergeV.items():
			nbr_v = np.sum(MergeC[k])
			nv = np.array(MergeC[k]).dot(np.array(MergeV[k]))/nbr_v
			M[k,:] = nv
			C[k] = nbr_v
		del MergeV,MergeC
		return M,C

