from lsa_job import str2val
import os
from lsa_job import load_data

class LSA(object):
	def __init__(self,oDir):
		super(LSA,self).__init__()
		self.oDir = oDir

	def clone(self,OBJ,sList=None):

		if sList is None:
			sList = OBJ.__dict__
		for fld in sList:
			setattr(self,fld,str2val(getattr(OBJ,fld)))

	def init_lsa(self):

		self.cMax = 2**self.cSize

		fname = self.oDir + self.sdrCLS + 'nbrHashes.txt'
		if os.path.isfile(fname):
			self.nbr_H = load_data(fname,fl='nbr')
			self.nbr_C = self.nbr_H / self.cMax
			if self.nbr_H % self.cMax != 0:
				self.nbr_C += 1

		self.sampleList = load_data(self.oDir + self.sdrJOB + 'sampleList.txt',fl='str')

		self.sampleHashFile = {}# sample_dict
		for i in range(self.nbr_S):
			self.sampleHashFile[i] = self.oDir + self.sdrHASH + self.sampleList[i] + '.count.hash'
