#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_names import get_file_name,get_dir_name,filtered_scaffolds_dir,report_spades_dir
from streaming_eigenhashes import StreamingEigenhashes

from lsa_job import *
from job_vars import JobVars
from counts_vector import Counts_Vector

help_message = 'usage example: python script_name.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        
	JV = JobVars(pDir)
	HO = StreamingEigenhashes(JV.oDir)

	NZI = load_data(HO.sampleHashFile[fr] + '.nzi',fl='list')
	s2dir = HO.oDir + HO.sdrTMP + 'smp/'

	H = Counts_Vector()
	H.add_IV(NZI,[1]*len(NZI))

	for i in range(HO.nbr_S):
		st = 2**i
		if fr % (2*st):
			break
		if (fr + st) >= HO.nbr_S:
			break

		ld = True
		while ld:
			try:
				I = load_data(s2dir + 'i.' + str(fr+st) + '.smp',fl='list')
				V = load_data(s2dir + 'v.' + str(fr+st) + '.smp',fl='list')
				ld = False
			except:
				time.sleep(HO.wts)
		H.add_IV(I,V)
	
	save_data(H.I,s2dir + 'i.' + str(fr) + '.smp',fl='list')
	save_data(H.V,s2dir + 'v.' + str(fr) + '.smp',fl='list')
