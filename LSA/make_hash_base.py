#!/usr/bin/env python

import sys,getopt,os,glob,time
from fastq_reader import Fastq_Reader
from lsa_job import save_data,check_slurm_pid,remove_file,remove_dir,run_slurm_job
from job_vars import JobVars

help_message = "usage example: python script_name.py -p /project/home/"
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)

	HO = Fastq_Reader(JV.oDir)
	print 'hashobject is created'
	fName = JV.path2hash_base_file()
	remove_file(fName)

	hash_base = HO.make_hash_base()
	print 'wheel is created'
	save_data(hash_base,fName, fl='mm')
	print 'wheel is saved',fName

