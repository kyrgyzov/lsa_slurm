#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from collections import defaultdict
from space_m import  mat_count_connects,uv_un_D,dict_row_col,merge_sparse

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir
	X = [load_data(read_dir + 'X_'+str(i),fl='mm',dump=np.float32) for i in range(HO.nbr_T)]
	VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r+', shape=(HO.nbr_H,))

	gm= HO.ms_m()
	print 'gm',gm
	uv,un,D = uv_un_D(VI)

	cl = uv.size
	print 'cls',cl

	FL = get_file_list(PATH=read_dir+'tCN/',END='*cn')
	CN = merge_sparse(FL,cl)
	mni=np.argmin(un)

	while un[mni]<=gm:
	    CN[mni,mni] = 0
	    v= dict_row_col(CN,mni)
	    mxi=np.argmax(v)
	    if v[mxi]:
		VI[uv[mni]==VI] = uv[mxi]
		un[mxi] += un[mni]
		uv[mni] = uv[mxi]
	    print 'mni,uv.size,un[mni],gm',mni,uv.size,un[mni],gm
	    un[mni] = 2*gm
	    mni=np.argmin(un)

	del VI
	
	HO.close_AM_files()		
