import os,time,glob
import warnings
import numpy as np
import cPickle as pk
from operator import itemgetter
from lsa_base import LSA
from lsa_job import *
from job_vars import JobVars
from scipy.spatial import distance
from lsa_ldh import ldh_mat
from lsa_names import id_generator
from random import randint

def numeral2int(fl,base = 0):
	sz = fl.shape
	val = [0] * sz[0]
	bs = 1
	for i in range(sz[1]):
		for j in range(sz[0]):
			val[j] +=  (int(fl[j,i])*bs)
		bs *= base
	return val

class Hyper_Sequences(LSA):

	def __init__(self,oDir):
		super(Hyper_Sequences,self).__init__(oDir)
		JV = JobVars(oDir)
		self.hash_base_file = JV.path2hash_base_file()
		self.dec_cls_dir = JV.dec_cls_dir()
		del JV

	def generator_to_coords(self,sequence_generator):
		for s in sequence_generator:
			if self.hName == 'hyper':
				coords = self.letters_to_coords(s)
			elif self.hName == 'gallager':
				coords = self.letters_to_int(s)
			else:
				raise NameError('Hash name is not defined')

			yield s['_id'],coords

	def letters_to_int(self,S):
		ltc = {'A': 1, 'C': 2,'G': 3, 'T': 4}
		return [ltc.get(l,0) for l in S['s']]
		
	def letters_to_coords(self,S):
		# THIS EQUATES A/T AND C/G MISMATCHES WITH NEGATIVE MATCHES, AND ALL OTHERS AS NON-MATCHES
		ltc_q = {'A': complex(-1,0),'T': complex(1,0),'C': complex(0,-1),'G': complex(0,1)}
		p_correct = 1-1./2000
		ltc_no_q = {'A': complex(-1,0)*p_correct,'T': complex(1,0)*p_correct,'C': complex(0,-1)*p_correct,'G': complex(0,1)*p_correct}
		if 'q' in S:
			return np.array([ltc_q.get(l,complex(0,0)) for l in S['s']])*self.quality_to_prob(S['q'])
		else:
			return [ltc_no_q.get(l,complex(0,0)) for l in S['s']]

	def quality_to_prob(self,Q):
		return np.array([1-10**(-q/10.) for q in Q])

	def make_hash_base(self):
		if self.hName == 'hyper':
			hash_base = self.make_wheel()

		elif self.hName == 'gallager':
			hash_base = ldh_mat(self.kMer,self.tLDH,self.mLDH)
		else:
			raise NameError('Hash name is not defined')
		return hash_base

	def make_wheel(self):

		p2vecs = self.oDir + self.sdrTMP + 'vec/'
		remove_dir(p2vecs)
		
		from job_vars import JobVars
		JV = JobVars(self.oDir)
		sname = 'MakeHashVectors'
		JV.jobID = sname
		jname,oname,ename = JV.make_slurm_job()
		del JV

		pid = run_slurm_job(jname)

		wheel = np.zeros((self.hSize,self.kMer+1),dtype=np.complex128)

		for nbr in xrange(self.hSize):
			print 'upload a vector for hash position',nbr
			wheel[nbr,:] = self.upload_vector()
		
		wheel = self.wheel_adjustment(wheel)

		save_data([''],p2vecs+'stop',fl='str')

		wait_for_slurm_jobs(pid,self.wts,job = sname)

		remove_dir(p2vecs)
		remove_dir(self.oDir + self.sdrTMP + 'rand_fastq/')

		return wheel

	def get_dist_vector(self,B):

		ln = len(B)

		sm = np.zeros(ln)
		for i in xrange(ln):
			r = distance.cdist(np.conjugate([B[i]]),B,'cosine')
			r[0][i] = 0
			sm += r[0]
		return sm

	def wheel_adjustment(self,wheel):
		warnings.filterwarnings('ignore')

		B=[w[:-1] for w in wheel]
		pM=np.sum(self.get_dist_vector(B))

		P = self.upload_vector()
		C = P.pop()

		B.append(P)

		print pM
		cmv =0
		mx_iters = min(self.hSize,self.kMer)
		while cmv < mx_iters:
			print 'iter',cmv,'---------------'
			sm = self.get_dist_vector(B)

			mi =np.argmin(sm)
			cM=np.sum(sm)-2*sm[mi]
			print mi,cM,pM
			if (cM > pM) and (mi < self.hSize):
				print '-----------------new better vector-----------------'
				cmv = 0
				pM = cM
				B[mi]=P
				wheel[mi,:-1]=P
				wheel[mi,-1] =C
			else:
				cmv += 1
			P = self.upload_vector()
			C = P.pop()
			B[-1]=P
		print cmv, cM, pM

		return wheel

	def load_wheel(self):

		if self.hName == 'hyper':

			wheel = load_data(self.hash_base_file,fl='mm',dump=np.complex128)
			wheel.shape = (self.hSize,self.kMer+1)
			Wc = wheel[:,-1]
			self.Wp = np.transpose(wheel[:,:-1])
			self.Re = np.real(Wc)
			self.Im = np.imag(Wc)
			self.We = self.Re*self.Re + self.Im*self.Im

			if self.hType == 'ternary':
				bVal = 3
			else:
				bVal = 2
			self.bVec = np.array([bVal**i for i in xrange(self.hSize)],dtype=object)

		elif self.hName == 'gallager':
			wheel = load_data(self.hash_base_file,fl='mm',dump=np.uint16)
			wheel.shape = (self.tLDH,self.mLDH)
			self.gM = np.zeros((self.kMer,self.mLDH),dtype=object)
			pr = [5**i for i in range(self.tLDH)]
			for i in range(self.mLDH):
				self.gM[wheel[:,i],i] = pr
		else:
			raise NameError('Hash name is not defined')
		del wheel

	
	def get_hash_value(self,F1,F2):
		if self.hType != 'ternary':
			B1 = F1.dot(self.bVec)
			B2 = F2.dot(self.bVec)
		if self.hType == 'ternary':
			FF = F2 + (F1 + 0)
			VAL = FF.dot(self.bVec)
			ml = float(2**self.hSize-1)/(3**self.hSize-1)
			VAL = [int(float(v) * ml) for v in VAL]
			VAL = np.array(VAL, dtype=object)

		elif self.hType == 'and':
			VAL = np.bitwise_and(B1,B2)

		elif self.hType == 'or':
			VAL = np.bitwise_or(B1,B2)

		elif self.hType == 'xor':
			VAL = np.bitwise_xor(B1,B2)

		elif self.hType == 'min':
			B = np.array([B1,B2])
			VAL = B[B.argmin(0),range(B.shape[1])]

		elif self.hType == 'max':
			B = np.array([B1,B2])
			VAL = B[B.argmax(0),range(B.shape[1])]

		elif self.hType == 'mean':
			VAL = 0.5 * (B1 + B2)

		elif self.hType == 'doubled':
			VAL = np.vstack((B1,B2)).T
		else:
			raise NameError('unknown name of the hash type: ' + self.hType)
		if VAL.ndim == 1:
			VAL = VAL.reshape((VAL.size,1))

		return VAL

	def coords_to_bins(self,C,reverse_compliments=True):
		C = np.array(C)

		if self.hName == 'hyper':
			L1 = np.dot(C,self.Wp)
			e = np.real(L1) * self.Re + np.imag(L1) * self.Im
			fl1 = e > self.We
			#fl1 = L1 > self.Wc

			if reverse_compliments:
				L2 = np.dot(C[:,::-1]*-1,self.Wp)
				e = np.real(L2) * self.Re + np.imag(L2) * self.Im
				fl2 = e > self.We

			else:
				fl2 = fl1 * 0
				fl2 = fl2.astype(bool)

			V = self.get_hash_value(fl1,fl2)
			del L1,L2

		elif self.hName == 'gallager':
			L1 = np.dot(C,self.gM)
			if reverse_compliments:
				L2 = np.dot((5-C[:,::-1])%5,self.gM)
			V = np.hstack((L1,L2))

			del L1,L2
		else:
			raise NameError('Hash name is not defined')

		del C
		return V

	def generator_to_bins(self,sequence_generator,rc=True):
		C = []
		A = []
		r = 0
		for a,c in self.generator_to_coords(sequence_generator):
			if len(c)<self.kMer:
				c = np.pad(c, (0,self.kMer - len(c)), 'constant', constant_values=complex(0,0))
			for i in range(len(c) - self.kMer + 1):
				A.append(r)
				C.append(c[i:i+self.kMer])
			r += 1
		del sequence_generator
		if A:
			return A,self.coords_to_bins(C,reverse_compliments=rc)
		else:
			return None,None

	def upload_vector(self):

		cont = True
		while cont:
			FL = glob.glob(self.oDir + self.sdrTMP + 'vec/*.vec')
			if FL:
				fName = FL[randint(0, len(FL)-1)]
				try:
					P = load_data(fName,fl='list',dump='pk')
					cont = False
					print 'file is uploaded:',fName

				except:
					print 'cannot upload file:',fName

		os.remove(fName)
		return P

	def generate_vector(self):
		id_name = id_generator(32)
		p2rkm = self.oDir + self.sdrTMP + 'rand_fastq/' + id_name + '.fastq'
		
		self.rand_kmers_for_wheel(p2rkm,nbr_vec=1)
		f = open_file(p2rkm)
		L = self.pick_leaf_noloc(self.kMer,f)
		f.close()
		P = self.affine_hull(L.values())

		p2vec = self.oDir + self.sdrTMP + 'vec/' + id_name + '.vec'
		save_data(P,p2vec,fl='list',dump='pk')
		os.remove(p2rkm)


	def pick_leaf_noloc(self,nodes,f):
		new_leaf = {}
		nl = [_ for _ in self.generator_to_coords(self.read_generator(f,max_reads=nodes))]
		for nlx in nl:
			new_leaf[len(new_leaf)] = list(nlx[1])
		return new_leaf

	def affine_hull(self,linear_system):
		# linear_system: d dimensions of n docs in this hyperplane
		for row in linear_system:
			row.append(-1)
		linear_system.append([0]*len(linear_system[0]))
		linear_system = np.array(linear_system)
		U,W,V = np.linalg.svd(linear_system)
		return list(V[-1,:])
