import os,random
from lsa_job import load_data,open_file,str_rep_dict,file_size,remove_dir

def load_script_code(p2scripts,script_name):
      Job = {}
      LS=[]
      job_found = False
      with open_file(p2scripts) as f:
            for raw_line in f:
                  line = raw_line.strip()
                  if job_found and (line != ''):
                        LS.append(line)
                  elif job_found and (line == ''):
                        break
                  if not job_found and (script_name == line):
                        job_found = True
      if not job_found:
            raise ValueError(script_name + ' is not found in' + p2scripts)

      Job['body'] = []
      for el in LS:
            if el[0] == '#':
                  pass
            elif el[:6] == 'array,':
                  pos = el.find(',')
                  Job[el[:pos]]=el[(pos+1):]
            else:
                  Job['body'].append(el)
      return Job

class JobCode():

	def __init__(self,p2code,job_name):

	    self.header = load_data(p2code + 'header.code',fl = 'str')
	    self.footer = load_data(p2code + 'footer.code',fl = 'str')

	    body = load_script_code(p2code + 'scripts.code',job_name)
            for key,value in body.items():
		    setattr(self,key,value)
	    del body


	def load():
            pass

	def write():
            pass

	def extend_Header(self,JobVars):
              extHeader=[]

              if hasattr(self,'array'):
                    AS,step = JobVars.get_array_size(self.array)
                    extHeader.append('#SBATCH --array=0-'+ str(AS-1))		
                    print 'with step ' + str(step)
                    print JobVars.jobID + ' array size will be 0-' + str(AS-1)

              if 'Spades_prt' in JobVars.jobID:
                    extHeader.append('#SBATCH -c cpuSpades')

              if 'MetaP_smp' in JobVars.jobID:
                    extHeader.append('#SBATCH -c cpuMetaP')

              if 'IlMap_' in JobVars.jobID:
                    extHeader.append('#SBATCH -n 1')
                    extHeader.append('#SBATCH -c taskIlMap')

              if 'Compress' in JobVars.jobID:
                    mem = file_size(JobVars.oDir + JobVars.sdrCLS + 'hash_indices')
                    mem *= 17
                    mem /= (2**20)
                    mem += 1

              if 'normal' == JobVars.sPart:
                    extHeader.append('#SBATCH -x etna7,etna52')

              if 'spams' in JobVars.decType:
                    if any([JobVars.jobID == v for v in ['DecomposeMatrix','ClusterCentroids','MakeSpamsChunks','MakeClusterChunks','ClusterChunks']]):
                          extHeader.append('module load spams 2>&1')

              if 'DecomposeMatrix' ==  JobVars.jobID and 'gensim'in JobVars.decType:
                    extHeader.append('export PYRO_SOCK_REUSE=True')
                    extHeader.append('export PYRO_SERIALIZERS_ACCEPTED=serpent,json,marshal,pickle')
                    extHeader.append('export PYRO_SERIALIZER=pickle')
                    extHeader.append('export PYRO_NS_HOST=localhost')
                    extHeader.append('export PYRO_NS_PORT=65431')
                    extHeader.append('export PYRO_HOST=localhost')
                    extHeader.append('rm -f oDir/sdrLOG/nameserver.log')
                    extHeader.append('rm -f oDir/sdrLOG/dispatcher.log')
                    extHeader.append('rm -f oDir/sdrLOG/worker*')
                    extHeader.append('p2python -m Pyro4.naming -n 0.0.0.0 > oDir/logs/nameserver.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_worker > oDir/logs/worker0.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_worker > oDir/logs/worker1.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_worker > oDir/logs/worker2.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_worker > oDir/logs/worker3.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_worker > oDir/logs/worker4.log 2>&1 &')
                    extHeader.append('p2python -m gensim.models.lsi_dispatcher > oDir/logs/dispatcher.log 2>&1 &')

              if 'BuildFastqParts' ==  JobVars.jobID:
                        remove_dir(JobVars.oDir + JobVars.sdrPART + JobVars.dec_cls_dir())
                        remove_dir(JobVars.oDir + JobVars.sdrREP + 'parts/' + JobVars.dec_cls_dir())

              extHeader.append('echo Date: `date`')		
              extHeader.append('t1=`date +%s`')		
		
              self.header.extend(extHeader)
              del extHeader

	def make_script(self,JobVars):
              self.extend_Header(JobVars)
              script = ''
              script += str_rep_dict('\n'.join(self.header), JobVars) + '\n'
              script += str_rep_dict('\n'.join(self.body), JobVars) + '\n'
              script += '\n'.join(self.footer) + '\n'
              return script

		
