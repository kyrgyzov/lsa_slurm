#!/usr/bin/env python
import sys,getopt,os
from streaming_eigenhashes import StreamingEigenhashes
from job_vars import JobVars
from lsa_job import *



help_message = 'usage example: python matrix_pd.py -p /project/home/'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

	JV = JobVars(pDir)
	
	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	save_data([ ','.join( [str(v) for v in np.sum(model==0,0)]) ],HO.oDir + HO.sdrREP + 'StatsDec/left_mat_zeros0.txt',fl='str')
	save_data([ ','.join( [str(v) for v in np.sum(model==0,1)]) ],HO.oDir + HO.sdrREP + 'StatsDec/left_mat_zeros1.txt',fl='str')
	

	JV = JobVars(HO.oDir)
	JV.jobID = 'StatsDecMatChunks'
	jname,oname,ename = JV.make_slurm_job()

	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,HO.wts,job = JV.jobID)

	path2save = HO.oDir + HO.sdrTMP + 'StatsDec/'
	
	CN = np.zeros((HO.nbr_T,2),dtype=np.uint64)
	FL = glob.glob(path2save+'*.npy')
	for fl in FL:
		tCN = load_data(fl,fl='npy')
		CN += tCN
	
	R=['comp, #non-zer, #non-neg, %non-zer, %non-neg']

	for i in range(HO.nbr_T):
		R.append(str(i)+', ' + str(CN[i,0])+', ' + str(CN[i,1])+', ' + str(float(CN[i,0])/HO.nbr_H)+', ' + str(float(CN[i,1])/HO.nbr_H))
	
	save_data(R,HO.oDir + HO.sdrREP + 'StatsDec/stats_dec_mat.txt',fl='str')

	remove_dir(path2save)
