#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars,array_size_step
from collections import defaultdict
from space_m import *

def slurm_cluster_mat(HO,jname):

	path2cls = HO.oDir + HO.sdrCLS + HO.dec_cls_dir + 'CLS/'

	scriptName = jname[(jname.rfind('/')+1):]

	prm = np.random.permutation(HO.nbr_C)
	save_data(prm,path2cls + 'prm.npy',fl='npy')

	pid = run_slurm_job(jname)
	while check_slurm_pid(pid):
		print time.ctime(), scriptName, 'job is running, wait ' + str(HO.wts) + ' s'
		time.sleep(HO.wts)
	print time.ctime(), scriptName,' is finished'

	AS,step = array_size_step(HO.nbr_C,HO.MAS)

	Centroids = np.zeros((0,HO.nbr_T))
	Counts = np.zeros(0,dtype=np.uint64)
	ii = 0

	for g in range(AS):
		fc = path2cls + 'CO.' + str(g) + '.npy'
		lCO = load_data(fc,fl='npy')
		os.remove(fc)
		fc = path2cls + 'CE.' + str(g) + '.npy'
		lCE = load_data(fc,fl='npy')
		os.remove(fc)

		Centroids,Counts = HO.merge_vectors(Centroids,Counts,lCE,lCO)
		ii += 1 

	return Centroids,Counts


help_message = 'usage example: python kmer_cluster_centroids.py -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)

	HO = StreamingEigenhashes(JV.oDir)

	HO.load_decomp_model()
	savedir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir

	remove_dir(savedir + 'SMP/')
	
	if HO.clsType == 'CosineMeans':

		JV.jobID = HO.clsType+'Chunks'
		jname,oname,ename = JV.make_slurm_job()

		ci = 0
		cVALS = np.zeros(0)

		nextStep = True
		while nextStep:
			ci += 1
			print ci,'iter --------------------------------------'

			Centroids,Counts = slurm_cluster_mat(HO,jname)
			print 'final clusters for the iteration',Centroids.shape
			sm = Counts.sum()
			prc = np.array([float(v)/sm for v in Counts ])
			Counts[:] = HO.nbr_H*prc

			flg = Counts > 0
			Centroids = Centroids[flg,:]
			Counts = Counts[flg]

			sr = np.argsort(Counts)[::-1]
			Centroids = Centroids[sr,:]
			Counts = Counts[sr]


			cNbr = Counts.size

			print 'the # of clusters :',cNbr

			if ci >= 3:
				if np.sum(np.abs(cVALS.astype(float) - cNbr)/cNbr<0.005):
					nextStep = False
			cVALS=np.append(cVALS,cNbr)
			print 'cVALS',cVALS

			save_data(Centroids,savedir + 'cluster_centroids.npy',fl='npy')
			save_data(Counts,savedir + 'cluster_counts.npy',fl='npy')
        	        #+1 cluster for imposters
			save_data(Counts.size+1, savedir+'nbrClusters.txt',fl='nbr')

	elif HO.clsType == 'mSpace':
		from shutil import copyfile

		read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir

		JV.jobID = HO.clsType+'ChunksTransf'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

		
		JV.jobID = HO.clsType+'ChunksMerge'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)
		remove_dir(read_dir + 'TR/')

		JV.jobID = HO.clsType+'IndMat'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

		IND = np.memmap(read_dir+'IND', dtype = np.uint64, mode='w+', shape=HO.szIndMat())
		for j in range(HO.nbr_C):
			strC = j*HO.cMax
			szC = min(HO.cMax,HO.nbr_H-strC)
			tIND = load_data(read_dir+'tIND/'+str(j)+'.npy', fl='npy')
			IND[:,strC:strC+szC] = tIND[:]
		del IND
		remove_dir(read_dir+'tIND/')

		#####################################

		JV.jobID = HO.clsType+'SearchM'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)
		SD = np.memmap(read_dir+'SD', dtype = np.float32, mode='w+', shape=(HO.nbr_H,))
		for j in range(HO.nbr_C):
			strC = j*HO.cMax
			szC = min(HO.cMax,HO.nbr_H-strC)
			tSD = load_data(read_dir+'tSD/'+str(j)+'.npy', fl='npy')
			SD[strC:strC+szC] = tSD[:]
		del SD
		remove_dir(read_dir+'tSD/')

		#####################################
		
		JV.jobID = HO.clsType+'SearchMnb'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)
		VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='w+', shape=(HO.nbr_H,))
		for j in range(HO.nbr_C):
			strC = j*HO.cMax
			szC = min(HO.cMax,HO.nbr_H-strC)
			tVI = load_data(read_dir+'tM/'+str(j)+'.npy', fl='npy')
			VI[strC:strC+szC] = tVI[:]

		uv,un,D = uv_un_D(VI)
		del VI
		remove_dir(read_dir+'tM/')

		save_data(uv.size, savedir+'nbrClusters_mnb.txt',fl='nbr')
		copyfile(read_dir+'VI', read_dir+'VI'+'_mnb')

		#####################################
		VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r+', shape=(HO.nbr_H,))
		for j in range(HO.nbr_H):
			#print j
			tI = np.array([j],dtype=np.uint64)
			while not np.any(tI == VI[tI[-1]]):
				tI = np.append(tI, VI[tI[-1]])
			VI[tI] = tI[-1]

		uv,un,D = uv_un_D(VI)
		del VI
		save_data(uv.size, savedir+'nbrClusters_m2l.txt',fl='nbr')
		save_data(uv.size, savedir+'nbrClusters.txt',fl='nbr')
		copyfile(read_dir+'VI', read_dir+'VI'+'_m2l')

		#####################################
		JV.jobID = HO.clsType+'Filter'
		jname,oname,ename = JV.make_slurm_job()
		scriptName = jname[(jname.rfind('/')+1):]
		pid = run_slurm_job(jname)
		wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

		VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='w+', shape=(HO.nbr_H,))
		for j in range(HO.nbr_C):
			strC = j*HO.cMax
			szC = min(HO.cMax,HO.nbr_H-strC)
			tVI = load_data(read_dir+'tFI/'+str(j)+'.npy', fl='npy')
			VI[strC:strC+szC] = tVI[:]
		del VI
		remove_dir(read_dir+'tFI/')

		#####################################
		VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r', shape=(HO.nbr_H,))
		uv,un,D = uv_un_D(VI)
		cnc = uv.size

		print 'nbr_H',HO.nbr_H
		print 'ini nbr_filt',uv.size
		print np.sum(un==1)
		print np.sort(un)
		del VI
		save_data(uv.size, savedir+'nbrClusters_f1.txt',fl='nbr')
		save_data(uv.size, savedir+'nbrClusters.txt',fl='nbr')

		if np.sum(un<=HO.ms_m()):

			JV.jobID = HO.clsType+'CountConnects'
			jname,oname,ename = JV.make_slurm_job()
			scriptName = jname[(jname.rfind('/')+1):]
			pid = run_slurm_job(jname)
			wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

			JV.jobID = HO.clsType+'MergeSmall'
			jname,oname,ename = JV.make_slurm_job()
			scriptName = jname[(jname.rfind('/')+1):]
			pid = run_slurm_job(jname)
			wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)

			VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r', shape=(HO.nbr_H,))
			uv,un,D = uv_un_D(VI)
			cnc = uv.size

			print 'nbr_H',HO.nbr_H
			print 'ini nbr no_small',uv.size
			print np.sum(un==1)
			print np.sort(un)
			del VI
			save_data(uv.size, savedir+'nbrClusters_no_small.txt',fl='nbr')
			save_data(uv.size, savedir+'nbrClusters.txt',fl='nbr')

		copyfile(read_dir+'VI', read_dir+'VI'+'_no_small')

		#####################################

		if HO.mS_mG:
			JV.jobID = HO.clsType+'MergeGauss'
			jname,oname,ename = JV.make_slurm_job()
			scriptName = jname[(jname.rfind('/')+1):]
			pid = run_slurm_job(jname)
			wait_for_slurm_jobs(pid,int(JV.wts),job = scriptName)
	
		
	elif HO.clsType == 'IndMax':
		save_data(HO.nbr_T, savedir+'nbrClusters.txt',fl='nbr')

	elif HO.clsType == 'Nzi':
		save_data(HO.nbr_T, savedir+'nbrClusters.txt',fl='nbr')

	else:
		raise NameError('Unknown clsType - ' + HO.clsType)
	


	remove_dir(savedir + 'CLS/')
