#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars

help_message = 'usage example: python kmer_cluster_chunk.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg
	
        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	HO = StreamingEigenhashes(JV.oDir)

	model= HO.load_decomp_model()

	save_dir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir + 'CLS/'
	Centroids = HO.load_clusters()

	HO.open_AM_files()

	HW = load_data(JV.hash_weight_file(),fl='mm',dump=np.float32)

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_C:
			break

		Hash_Cluster,CC,CP = HO.model_cluster_chunk(ind,model,Centroids,HW)

		lpath = save_dir + extend_path(str(ind)) + '/'
		save_data(Hash_Cluster, lpath + 'hash_cluster.list',fl='list')
		save_data(CC, lpath  + 'CC.list',fl='list')
		save_data(CP, lpath  + 'CP.npy',fl='npy')

		print HO.clsType,', ind - ', ind,', sizes - ',len(Hash_Cluster),len(CC)

	HO.close_AM_files()		
