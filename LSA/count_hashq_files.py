#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from fastq_reader import Fastq_Reader
from lsa_job import *
from job_vars import JobVars
from counts_vector import Counts_Vector

help_message = 'usage example: python merge_count_fractions.py -r 3 -p /project/home'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-r','--filerank'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')
	AS,step = JV.get_array_size('files')

	HO = Fastq_Reader(JV.oDir)
	
	gH = Counts_Vector()
	cName = ''

	for i in range(step):
		ind = step*fr + i
		t1 = time.time()

		if ind >= HO.nbr_F:
			break
		file_name = FP[ind]
		tH = HO.hash_counts_for_file(file_name)
		fname = get_file_name(file_name)
		sample_name = get_prefix_from_name(fname)
		print fr,i,ind, time.time() - t1,FP[ind]

		if not cName:
			cName = sample_name

		if sample_name !=  cName:
			cDict = JV.oDir + JV.sdrHASH  + cName + '/DICT/'
			dfile = cDict + cName +'.' + str(fr)
			print dfile

			save_data(gH.I,dfile + '.i',fl='list')
			save_data(gH.V,dfile + '.h',fl='list')
			del gH
			gH = Counts_Vector()
			cName = sample_name
		gH.add(tH)
		del tH

	if cName:
		cDict = JV.oDir + JV.sdrHASH  + cName + '/DICT/'

		dfile = cDict + cName +'.' + str(fr)
		print dfile
		save_data(gH.I,dfile + '.i',fl='list')
		save_data(gH.V,dfile + '.h',fl='list')
		del gH
