#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from gensim import models
import time
from scipy.spatial import distance
import math
from job_vars import JobVars
from lsa_math import safe_cosine
import matplotlib.pyplot as plt
import random

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')
	HO = StreamingEigenhashes(JV.oDir)

	svDir = JV.oDir + JV.sdrCLS

	fName = svDir + 'F.npy'
	F = load_data(fName,fl='npy')

	fName = svDir + 'C.c'
	C = load_data(fName,fl='list')

	eps = np.finfo(F.dtype).eps
	m = int(math.sqrt(HO.nbr_H))

	model= HO.load_decomp_model()

	HASH = np.empty((HO.nbr_H,HO.nbr_T),dtype=np.float32)
	for k in xrange(HO.nbr_C):
		tmp_block = load_data(JV.oDir + JV.sdrCLS + 'VEC/' + extend_path(str(k)) + '.npy',fl='npy')
		HASH[k*HO.cMax:k*HO.cMax+tmp_block.shape[1],:] = tmp_block.T
	print HASH.shape

	vc= np.zeros((1,HO.nbr_T),dtype=np.float32)

	gcl = 0


	for k in xrange(HO.nbr_H):
		if C[k] != -1:
			continue

		vc[0,:] = HASH[k,:]
		tD = safe_cosine(vc, HASH, HO.clThresh)[0]
		sI = np.argsort(tD)
		#############
		sI=sI[:(2*HO.nbr_T)]
		tF = F[sI]
		if tF[sI == k] == tF.max():
			print k
		#############
		gcl += 1

	print 'gcl',gcl

	fName = svDir + 'C.c'
	save_data(C,fName,fl='list')
