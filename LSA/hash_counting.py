#from bitarray import bitarray
from ctypes import c_uint16
import glob,os
import numpy as np
import scipy.stats as stats
import gzip
from lsa_base import LSA
from lsa_job import *
from lsa_names import *
from counts_vector import Counts_Vector

class Hash_Counting(LSA):

	def __init__(self,oDir):
		super(Hash_Counting,self).__init__(oDir)

	def hash_counts_for_file(self,file_name):

		fname = get_file_name(file_name)
		sample_name = get_prefix_from_name(fname)
		cname = cut_sufix_from_name(fname,sfx='.fastq')

		hfile = self.oDir + self.sdrHASH + sample_name + '/' + cname +'.hashq.gz'

		dvd = 2 if self.paired_reads else 1

		hGen = self.read_from_hashq(hfile)
		LS=[]
		D = {}

		r_id = 0
		for ls in hGen:
			for v in ls:
				D[v] = 1
			if (r_id + 1) % dvd ==0:
				LS += D.keys()
				D = {}
			r_id += 1

		H = Counts_Vector()
		H.make_hist(LS)
		return H
		

	def hash_counts_for_sample_add_dicts(self,sample_name):

		H = Counts_Vector()
		path2sample = self.oDir + self.sdrHASH +  sample_name
		path2dict = path2sample + '/DICT/'
		FP = glob.glob(path2dict + sample_name + '.*.h')

		if len(FP) == 0:
			raise NameError('no files found in:\n ' + path2dict)

		for fp in FP:
			lH = load_data(fp,fl='list')
			lI = load_data(fp[:-1]+'i',fl='list')
			H.add_IV(lI,lH)
			del lH,lI

		return H

	def hash_counts_for_sample(self,sample_name):
		tp=np.uint16
		type_max = np.iinfo(tp).max

		H = np.zeros(self.hash_max,dtype=tp)
		mask2files = self.oDir + self.sdrHASH + sample_name + '/*.hashq.gz'

		FP = glob.glob(mask2files)
		if len(FP) == 0:
			raise NameError('no files found in:\n ' + mask2files)

		for fp in FP:
			LS = load_data(fp,fl='list')
			for a in LS:
				for b in a:
					if H[b] < type_max:
						H[b] += 1

		nzi = np.nonzero(H)[0]
		H = H[nzi]

		ln=len(H)
		X = np.memmap(self.oDir + self.sdrHASH + sample_name +'.count.hash',dtype=H.dtype,mode='w+',shape=(ln,))
		X[:]=H
		del X
		X = np.memmap(self.oDir + self.sdrHASH + sample_name +'.count.hash.nzi',dtype=nzi.dtype,mode='w+',shape=(ln,))
		X[:]=nzi
		del X

		print 'sample %s has %d nonzero elements and %d total observed kmers with sparseness %f' % (sample_name,ln,H.sum(),1-np.float(ln)/(self.hash_max))

	def bitarray_from_array(self,A):
		H = bitarray(2**self.hSize)
		H.setall(False)
		for a in A:
			H[a] = True
		return H
