#!/usr/bin/env python
import sys,os,glob
import getopt,gzip
from lsa_job import *
from lsa_names import *
from job_vars import JobVars


if __name__ == "__main__":
        try:
                opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
        except:
                print help_message
                sys.exit(2)

        for opt, arg in opts:
                if opt in ('-h'):
                        print help_message
                        sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)

	pfx = JV.PFX
	sfx = ''

	mrg = int(JV.cReads)

	FP = load_data(JV.oDir + '/job/sampleList.txt',fl='str')
	if type(FP) is not list:
		FP = [FP]
	AS,step = JV.get_array_size('samples')


	for i in range(step):
		ind = step*fr + i
		if ind >= len(FP):
			print fr,step,i,ind, len(FP)
			break

		sname=FP[ind]

		print 'inputdir: ', JV.iDir , ', output: ', JV.oDir, ', sname: ', sname,', sfx: ',sfx

		FL = get_file_list(JV.iDir,sname+'.',sfx,'*fastq*')
		FL.sort()
		paired = 1
		if len(FL)>1:
			df1,df2 = str_diff(FL[0],FL[1])
			if len(df1)==1 and len(df2)==1:
				paired = 2
		reads_written = 0
		bytes_written = 0
		cf = 0
		for i in range(0,len(FL),paired):
			pair1 = FL[i]
			f1 = open_file(pair1)
			f2 = None
			
			fname1 = cut_sufix_from_name(get_file_name(pair1),'.fastq')
			if paired == 1:
				fname = fname1
			else:
				pair2 = FL[i+1]
				f2 = open_file(pair2)
				fname2 = cut_sufix_from_name(get_file_name(pair2),'.fastq')
				fname = str_comm_head(fname1,fname2) 

			ch,rw,bw = convert_compress_pairs(JV.oDir+JV.sdrSAMPLES,sname,fname,f1,f2,mrg)
			reads_written += rw
			bytes_written += bw
			
			f1.close()
			if f2 !=  None:
				f2.close()
			cf += 1

		print sname, ', fr - ', fr, ', cf - ',cf, ', ch - ', ch, ', reads_written - ', reads_written, ', bytes_written - ',bytes_written
		print '------------------------------------------'
