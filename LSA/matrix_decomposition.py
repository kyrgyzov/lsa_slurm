import getopt,gc,os,time,sys
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
import multiprocessing
from functools import partial
import time
from job_vars import *
import subprocess
from lsa_base import LSA

def pool_project(params):
	g = params[0]
	T  = params[1]
	u  = params[2]
	nn = params[3]
	path2chk = params[4]

	ln = T.shape[0]
	nu = np.zeros(ln)
	l = 0.0

	fname = path2chk + extend_path(str(g)) + '.chk'
	LS = load_data(fname,fl='list')
	rs = np.zeros(ln)
	for vec in LS:
		if vec:
			rs.fill(0.0)
			for v in vec:
				rs += (T[:,v[0]]*v[1])
			if nn:
				rs[rs<0]=0.0
			a = sum(u*rs)
			l += a*a
			nu += a*rs

	return (l,nu)

def pool_chunk_cov(params):
	g = params[0]
	T  = params[1]
	nn = params[2]
	path2chk = params[3] + 'CHK/'
	path2cov = params[3] + 'COV/'

	ln = T.shape[0]

	fname = path2chk + extend_path(str(g)) + '.chk'
	LS = load_data(fname,fl='list')
	rs = np.zeros(ln)
	lC = np.zeros((ln,ln))

	for vec in LS:
		if vec:
			rs.fill(0.0)
			for v in vec:
				rs += (T[:,v[0]]*v[1])
			if nn:
				rs[rs<0]=0.0
			lC += np.outer(rs,rs)


	fname = path2cov + extend_path(str(g)) + '.npy'
	save_data(lC,fname,fl='npy')

def pool_cov_mat(HO,pool,T):

	path2hash = HO.oDir + HO.sdrHASH
	nn = 'nmf' in HO.decType

	print 'pool._processes - ',pool._processes
	oList=[(g,T,nn,path2hash) for g in xrange(HO.nbr_C)]
	REZ = pool.map(pool_chunk_cov, oList)

	COV = np.zeros((HO.nbr_S,HO.nbr_S))
	for g in xrange(HO.nbr_C):
		fc = path2hash + 'COV/' + extend_path(str(g)) + '.npy'
		lC = load_data(fc,fl='npy')
		os.remove(fc)
		COV += lC

	del oList, REZ
	#del pool, oList, REZ
	#gc.collect()

	return COV

def slurm_cov_mat(HO,jname):

	path2cov = HO.oDir + HO.sdrTMP + 'COV/'
	scriptName = jname[(jname.rfind('/')+1):]

	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,HO.wts,job = scriptName)

	AS,step = array_size_step(HO.nbr_C,HO.MAS)

	COV = np.zeros((HO.nbr_S,HO.nbr_S))
	print HO.nbr_C
	for g in xrange(AS):
		fc = path2cov + str(g) + '.npy'
		lC = load_data(fc,fl='npy')
		os.remove(fc)
		COV += lC

	return COV

def mean_U_eq(GM,I):

	nb = int(np.max(I))+1
	U = np.zeros((GM.shape[0],nb),dtype=np.float32)

	for vi in range(nb):
		li = I==vi
		mv = np.sum(GM[:,li],1)
		nr = np.sqrt(np.sum(mv**2))
		if nr:
			mv /= nr
		else:
			print vi,'nr',nr
		U[:,vi] = mv[:]
	return U

def closest_U_GM(GM,U):

	nv = U.shape[1]
	ln = GM.shape[1]
	I = np.zeros(ln,dtype=np.uint64)
	F = np.ones(ln,dtype=bool)
	P = np.array(range(ln))
	nr = ln/nv

	for j in range(nv):
		pr = 1 - np.dot(U[:,j],GM[:,F])
		tP = P[F]
		si = np.argsort(pr)[:nr]
		I[tP[si]] = j
		F[tP[si]] = False
	return I


def slurm_spams_mat(HO,jname):

	path2spams = HO.oDir + HO.sdrTMP + 'spams/'
	scriptName = jname[(jname.rfind('/')+1):]

	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,HO.wts,job = scriptName)

	FL = glob.glob(path2spams + 'c_*.npy')
	AS = len(FL)
	print 'nbr files',AS

	GM = np.zeros((HO.nbr_S,AS*HO.decRank),dtype=np.float32)
	I = np.zeros(AS*HO.decRank,dtype=np.uint64)
	for g in xrange(AS):
		lU = load_data(FL[g],fl='npy')
		lU[np.invert(np.isfinite(lU))] = 0.0
		GM[:,g*HO.decRank:(g+1)*HO.decRank] = lU
		I[g*HO.decRank:(g+1)*HO.decRank]=range(HO.decRank)

	Z = (GM == 0)
	ZV = np.sum(Z,0)

	pI=np.zeros(AS*HO.decRank,dtype=np.uint64)
	U = np.zeros((HO.nbr_S,HO.decRank),dtype=np.float32)
	if os.path.isfile(path2spams + 'T.npy'):
		U = load_data(path2spams + 'T.npy',fl='npy')
	else:
		U =  mean_U_eq(GM,I)
	I = closest_U_GM(GM,U)

	df = 1
	pdf = -1
	next=True
	i = 0
	md = GM.shape[1]

	while df and next:
		pdf = df
		pI[:] = I[:]

		U =  mean_U_eq(GM,I)
		I = closest_U_GM(GM,U)
		df = np.sum(pI!=I)

		print 'df',df,'md',md
		if (pdf == df) or (df == md):
			next = False
		if md>df:
			md = df

	TT = np.zeros(HO.decRank,dtype=np.uint32)
	for vi in range(HO.decRank):
		li = I==vi
		nz = int(np.mean(ZV[li]))
		if nz:
			nz -=1
		TT[vi] = nz
		zv = np.sum(Z[:,li],1)
		si = np.argsort(zv)[::-1]
		mv = U[:,vi]
		mv[si[:nz]] = 0.0

		nr = np.sqrt(np.sum(mv**2))
		mv /= nr
		U[:,vi] = mv[:]
	print TT
	return U


def slurm_vec_err_covs(HO,jname):

	path2cov = HO.oDir + HO.sdrTMP + 'COV/'
	scriptName = jname[(jname.rfind('/')+1):]

	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,HO.wts,job = scriptName)

	AS,step = array_size_step(HO.nbr_C,HO.MAS)

	COV = np.zeros((HO.nbr_S,HO.nbr_S,2))

	for g in xrange(AS):
		fc = path2cov + str(g) + '.npy'
		lC = load_data(fc,fl='npy')
		os.remove(fc)
		COV += lC

	return COV

	
def power_decomp_stream(oDir, rt = 1e-4):

	HO = StreamingEigenhashes(oDir)
	path2chk = HO.oDir + HO.sdrHASH + 'CHK/'
	pool = multiprocessing.Pool()
	print 'pool._processes - ',pool._processes

	U = np.zeros((HO.nbr_S,HO.nbr_S),dtype=np.float32)
	L = np.zeros(HO.nbr_S,dtype=np.float32)
	L[0] = 1

	l = 1.0
	r = 0
	nn = 'nmf' in HO.decType
	thr_iter = (HO.nbr_S)**2

	while (abs(l)>0.0) and (r<HO.nbr_S):
		print 'r - ',r
		T = np.eye(HO.nbr_S) - np.dot(U,np.linalg.pinv(U))
		u = np.ones(HO.nbr_S)
		pl = 0.0
		l = 1.0
		#rs = np.zeros(HO.nbr_S)
		i =0
		while (abs(l)>0.0) and (abs(abs(pl)-abs(l))>1e-10) and (i < (thr_iter)):
			i += 1

			pl = l
			oList=[(g,T,u,nn,path2chk) for g in range(HO.nbr_C)]
			RR = pool.map(pool_project, oList)
			l = np.sum([val[0] for val in RR ])
			nu=np.sum([val[1] for val in RR ],axis=0)
			u = nu
			nr = np.linalg.norm(u)
			if nr > 0.0:
				u /= nr
			print 'l - ',l
		print abs(l)
		print np.linalg.norm(L)
		print abs(l)/np.linalg.norm(L)

		if abs(l)/np.linalg.norm(L) > rt:
			U[:,r] = u
			L[r] = l
		else:
			l = 0.0
		r += 1
		print L
		print U
	r = sum(L>0.0)
	U = U[:,:r]
	L = L[:r]

	return U,L

def power_rank1_decomp_symetric_mat(M):

	ln = M.shape[0]
	u = np.ones(ln)
	thr_iter = ln**2

	pl = 0.0
	l = 1.0
	i = 0
	while (abs(abs(pl)-abs(l))>1e-10) and (i < (thr_iter)):
		i += 1
		pl = l
		u = np.dot(u,M)
		l = np.linalg.norm(u)
		if l > 0.0:
			u /= l
		l = np.dot(u,np.dot(u,M))
		l = np.sqrt(l)

	print 'thr_iter - ',thr_iter,', i - ',i

	return u,l

def decomp_stream(HO, rt = 1e-2):

	JV = JobVars(HO.oDir)
	JV.jobID = 'MakeCovChunks'
	jname,oname,ename = JV.make_slurm_job()

	ln = HO.nbr_S

	U = np.zeros((ln,HO.decRank),dtype=np.float32)
	L = np.zeros(HO.decRank,dtype=np.float32)
	L[0] = 1

	l = 1.0
	r = 0
	while (abs(l)>0.0) and (r<HO.decRank):
		print '---------------------'
		print time.ctime(),'start rank-1 decomposition',r

		T = np.eye(HO.nbr_S) - np.dot(U,np.linalg.pinv(U))
		save_data(T, HO.oDir + HO.sdrCLS + 'T.npy',fl='npy')
		M = slurm_cov_mat(HO,jname)

		print time.ctime(),'cov mat is readey',r
		u,l= power_rank1_decomp_symetric_mat(M)

		print time.ctime(),'cov mat is decomposed',r,',l - ',l
		if abs(l)/np.sum(np.sum(L)) > rt:
			U[:,r] = u
			L[r] = l
		else:
			l = 0.0
		r += 1

	#del pool
	remove_file(HO.oDir + HO.sdrCLS + 'T.npy')
	remove_dir(HO.oDir + HO.sdrHASH + 'COV/')

	r = sum(L>0.0)
	U = U[:,:r]
	L = L[:r]
	
	return U,L

def decomp_spams(HO):

	JV = JobVars(HO.oDir)
	JV.jobID = 'MakeSpamsChunks'
	jname,oname,ename = JV.make_slurm_job()

	ln = HO.nbr_S

	U = np.zeros((HO.nbr_S,HO.decRank),dtype=np.float32)

	pdf= 2.0
	df = 1.0
	tol = 1e-6
	
	k = 0
	while (pdf > df) and (np.abs(pdf-df)>tol) and (k < HO.nbr_S):
		print '---------------------'
		print time.ctime(),'start spams decomposition',k,HO.nbr_S
		k+=1
		pdf = df

		nU = slurm_spams_mat(HO,jname)
		df = np.mean(np.abs(nU-U))
		print 'df',df
		U = nU
		save_data(U, HO.oDir + HO.sdrTMP + 'spams/' + 'T.npy',fl='npy')

	remove_file(HO.oDir + HO.sdrTMP + 'spams/' + 'T.npy')
	remove_dir(HO.oDir + HO.sdrTMP + 'spams/')

	return U

def get_en_thr(v,rtv=0.01):

	sr = np.sort(v)
	cr = np.cumsum(sr)
	cr = cr/cr[-1]
	zei = np.nonzero(cr>rtv)[0]
	thr = sr[zei[0]]
	return thr

def adjust_converged(HO,jname,U,L,iters = -1):

	if iters < 0 :
		iters = U.shape[0]*U.shape[1]

	eps = 1 - 0.05;
	next = True
	r = 0
	while next:

		print '---------------------'
		print time.ctime(),'start atom adjustement',r
		next = False

		save_data(U, HO.oDir + HO.sdrCLS + 'T.npy',fl='npy')
		M = slurm_vec_err_covs(HO,jname)
		print 'U.shape',U.shape,'M.shape',M.shape,M.shape[-1]

		print time.ctime(),'cov mat is ready',r
		pu = np.copy(U[:,0])

		u,l= power_rank1_decomp_symetric_mat(M[:,:,0])
		U[:,0] = u
		L[0] = l

		vrt = np.dot(pu,U[:,0])
		print 'vrt'
		print vrt

		print np.sum(vrt),'U.shape',U.shape,eps
		r += 1
		if not next:
			print 'next is False'
			next = (r < iters) and (vrt < eps)
			print 'next',next
		print 'L',L.size,L

	u,l= power_rank1_decomp_symetric_mat(M[:,:,-1])
	L = np.append(L,l)
	u.shape=(u.size,1)
	U = np.append(U,u,axis=1)

	return U,L

def adjust_first_vector(HO,jname,U,L):


	print 'adjust_first_vector---------------------'

	save_data(U, HO.oDir + HO.sdrCLS + 'T.npy',fl='npy')
	M = slurm_vec_err_covs(HO,jname)
	print 'U.shape',U.shape,'M.shape',M.shape,M.shape[-1]
		
	u,l= power_rank1_decomp_symetric_mat(M[:,:,0])
	U[:,0] = u
	L[0] = l

	u,l= power_rank1_decomp_symetric_mat(M[:,:,-1])
	L = np.append(L,l)
	u.shape=(u.size,1)
	U = np.append(U,u,axis=1)

	return U,L

def sparsek_stream(HO):

	JV = JobVars(HO.oDir)
	JV.jobID = 'MakeVecErrCovsChunks'
	jname,oname,ename = JV.make_slurm_job()

	ln = HO.nbr_S

	U = np.ones((ln,1))
	U[:,0] /= np.sqrt(ln)
	L = np.zeros(1)


	eps = 0.05;
	
	for i in range(HO.decRank):
		print i,'make a vector'
		U,L = adjust_first_vector(HO,jname,U,L)
		print 'L',L
		U = np.roll(U,1,axis = 1)
		L = np.roll(L,1)

	U =U[:,:-1]
	L =L[:-1]
	r= 0

	SM = np.zeros(U.shape[1])
	next = True
	while next:
		
		mi = np.argmin(SM)
		print 'r,mi,SM',r,mi,SM

		U = np.roll(U,-mi,axis = 1)
		L = np.roll(L,-mi)
		SM = np.roll(SM,-mi)
		print 'r,mi,SM',r,mi,SM

		pu = np.copy(U[:,0])
		U,L = adjust_first_vector(HO,jname,U,L)
		U =U[:,:-1]
		L =L[:-1]
		SM[0] = np.dot(pu,U[:,0])
		print 'r,SM',r,SM
		print 'L',L
		print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
		print 'r,U.shape,np.sum(SM),U.shape[1]* eps',r,U.shape,np.sum(SM),U.shape[1]* (1-eps)
		r =+1
		next = np.any(SM < (1-eps))
		print 'next',next


	remove_file(HO.oDir + HO.sdrCLS + 'T.npy')
	remove_dir(HO.oDir + HO.sdrTMP + 'COV/')


	return U


def sparsek_stream_slow(HO, rt = 1e-2):

	JV = JobVars(HO.oDir)
	JV.jobID = 'MakeCovChunks'
	jname,oname,ename = JV.make_slurm_job()

	ln = HO.nbr_S

	U = np.random.rand(ln,HO.decRank)
	if 'svd' in HO.decType:
		U -= 0.5
	for j in range(HO.decRank):
		U[:,j] /= np.linalg.norm(U[:,j])

	r = 0
	while (r< 3* HO.decRank):
		print '---------------------'
		print time.ctime(),'start atom adjustement',r

		save_data(U, HO.oDir + HO.sdrCLS + 'T.npy',fl='npy')
		M = slurm_cov_mat(HO,jname)

		print time.ctime(),'cov mat is readey',r
		u,l= power_rank1_decomp_symetric_mat(M)
		U[:,0] = u
		U = np.roll(U,1,axis = 1)

		r += 1
		
	remove_file(HO.oDir + HO.sdrCLS + 'T.npy')
	remove_dir(HO.oDir + HO.sdrTMP + 'COV/')


	return U

def count_value_for_square_cov_mat(params):
	sm1 = params[0]
	sm2 = params[1]
	SD = params[2]

	ln = len(SD)
	v = 0.0

	if sm1<=sm2:
		bI = np.memmap(SD[sm1] + '.nzi',dtype=np.uint64,mode='r')
		bH = np.memmap(SD[sm1] + '.cond',dtype=np.float32,mode='r')
		L1 = len(bI)
		I = np.memmap(SD[sm2] + '.nzi',dtype=np.uint64,mode='r')
		H = np.memmap(SD[sm2] + '.cond',dtype=np.float32,mode='r')
		i1 = 0
		i2 = 0
		L2 = len(I)

		while (i1 < L1) and (i2 < L2):
			if bI[i1] == I[i2]:
				v += (bH[i1]*H[i2])
				i1 += 1
				i2 += 1
			else:
				if bI[i1] < I[i2]:
					i1 += 1
				else:
					i2 += 1

	return (v,)

def make_square_cov_mat(SD):

	ln = len(SD)
	print 'total number of samples - ',ln
	M = np.zeros((ln,ln),dtype=np.float32)

	oList=[(k/ln,k%ln,SD) for k in range(ln*ln)]
	pool = multiprocessing.Pool()
	RR = pool.map(count_value_for_square_cov_mat,oList)
	for k in range(ln*ln):
		i = k/ln
		j = k%ln
		if (i<=j):
			M[i,j] = RR[k][0]
			M[j,i]= M[i,j]
	return M

def sum_square_cov_mat(HO):
    D = np.zeros((HO.nbr_S,HO.nbr_S))
    print 'make a global cov matrix,', time.ctime()
    for ind in range(HO.nbr_C):
        fname = HO.oDir + HO.sdrHASH +'CHK/' + extend_path(str(ind)) + '.npy'
        tM = load_data(fname,fl='npy')
        D += tM
    print 'a global cov matrix was created,', time.ctime()
    return D


class MatrixDecomposition(LSA):

    def __init__(self,oDir):
	    super(MatrixDecomposition,self).__init__(oDir)

	    JV = JobVars(oDir)
	    self.decomp_file = JV.path2decomp_file()
	    self.cond_file = JV.cond_file()
	    self.hMax = JV.hash_max()
	    self.clone(JV)
	    self.init_lsa()

	    del JV

    def decompose(self,HO):
	    print 'model',self.decType
	    print 'rank',self.decRank
	    print 'decomp_file',self.decomp_file

	    if self.decType == 'none':
		    print '--------------'
		    print 'eye left matrix'
		    print 'diag ones for eigen values'
		    print 'rank - ',HO.nbr_S
		    save_data(['we do not use decomposition model','use initial data'],self.decomp_file,fl='str')

	    elif 'gensim' in self.decType:
		    import logging
		    from gensim import models

		    cMax = 2**self.cSize

		    HO.open_AM_files()

		    corpus =  HO.get_gen_hash(full=False)
		    print 'corpus is created'

		    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO,stream=sys.stdout)
		    lsi = models.LsiModel(corpus,num_topics = HO.nbr_S, id2word=HO.SD, distributed=True, chunksize=cMax)

		    HO.close_AM_files()		

		    lsi.save(self.decomp_file)
		    u = lsi.projection.u
		    s = lsi.projection.s
		    save_data(u,self.decomp_file+'.u.npy',fl ='npy')
		    save_data(s,self.decomp_file+'.s.npy',fl ='npy')

	    	    print 'eigen values'
		    print s
    		    print 'rank - ',len(s)


	    elif 'spams' in  self.decType:

    		    U = decomp_spams(HO)

		    save_data(U,self.decomp_file+'.U.npy',fl ='npy')
		    
		    print '--------------'
		    print 'U.shape',U.shape
		    print '--------------'

	    elif 'sklearn' in  self.decType:

		    import scipy.sparse as spr
		    print 'start sklearn'
		    HO.open_AM_files()
		    print 'hash files were opened'
		    
		    V = spr.csc_matrix((HO.nbr_H,0), dtype = np.float64)
		    print 'V'
		    for i in range(HO.nbr_S):
			    tV = spr.csc_matrix((HO.CFL[i], (HO.IFL[i], np.zeros(HO.IFL[i].size))), shape=(self.nbr_H, 1))
			    V=spr.hstack((V,tV))
			    print i,V.shape,V.size
		    HO.close_AM_files()


		    if self.decType == 'sklearn.svd':
			    from sklearn.decomposition import TruncatedSVD
			    rank = HO.decRank
			    if rank > HO.nbr_S-1:
				    rank = HO.nbr_S-1
			    model = TruncatedSVD(n_components = rank)
		    elif self.decType == 'sklearn.nmf':
			    from sklearn.decomposition import NMF
			    model = NMF(solver='cd',verbose=True,n_components = HO.decRank)
		    else:
			    raise NameError('unknown name of model ' + self.decType)

		    model.fit(V)
		    del V

		    print '--------------'
		    print 'model.components_.shape',model.components_.shape
		    

		    save_data(model,self.decomp_file,dump ='pk')


	    elif 'power' in self.decType:
		    print '# of chunks',HO.nbr_C
		    U,L = decomp_stream(HO)
		    print '--------------'
		    print L
		    print 'rank - ',len(L)
		
		    save_data(U, self.decomp_file + '.u.npy',fl='npy')
		    save_data(L, self.decomp_file + '.s.npy',fl='npy')

	    elif 'sparsek' in self.decType:
		    print 
		    print '# of chunks',HO.nbr_C
		    U = sparsek_stream(HO)
		    print '--------------'
		    print 'shape - ',U.shape
		
		    save_data(U, self.decomp_file + '.u.npy',fl='npy')

	    else:
		    raise NameError('unknown name of model ' + self.decType)
	    
