#!/usr/bin/env python
import sys,getopt,os
from matrix_decomposition import MatrixDecomposition
from streaming_eigenhashes import StreamingEigenhashes
from job_vars import JobVars
from lsa_job import *



help_message = 'usage example: python matrix_pd.py -p /project/home/'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

	JV = JobVars(pDir)
	
	HO = StreamingEigenhashes(JV.oDir)

	path2save = HO.oDir + HO.sdrCLS

	if not os.path.isfile(JV.hash_weight_file()):
		HW = HO.get_hash_weights()
		print 'JV.hash_weight_file()',JV.hash_weight_file()
		save_data(HW,JV.hash_weight_file(),fl='mm')

	if not os.path.isfile(path2save + 'hash_row_size'):
		MN,SD,NR,SZ = HO.get_hash_row_stats()
		save_data(MN,path2save + 'hash_row_mean',fl='mm')
		save_data(SD,path2save + 'hash_row_std',fl='mm')
		save_data(NR,path2save + 'hash_row_norm'+ str(HO.rn_p),fl='mm')
		save_data(SZ,path2save + 'hash_row_size',fl='mm')

	if HO.fullCM:
		print 'we create full cond matrix'
		HO.make_full_cond()
	else:
		HO.make_sps()

	DO = MatrixDecomposition(JV.oDir)
	DO.decompose(HO)

	'''
	JV.jobID = 'StatsDecMat'
	jname,oname,ename = JV.make_slurm_job()
	pid = run_slurm_job(jname)
	wait_for_slurm_jobs(pid,HO.wts,job = JV.jobID)
	'''
