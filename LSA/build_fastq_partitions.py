#!/usr/bin/env python

import sys, getopt,resource
import gzip, shutil
import glob,os,time
from lsa_job import *
from fastq_reader import Fastq_Reader
import numpy as np
from job_vars import JobVars
from collections import defaultdict
from datetime import datetime

help_message = 'usage example: python merge_partition_parts.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:c:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	FASTQ = load_data(JV.oDir +JV.sdrJOB + 'fileList.txt',fl='str')
	SAMPLES = load_data(JV.oDir +JV.sdrJOB + 'sampleList.txt',fl='str')

	inputdir =  JV.oDir + JV.sdrCLS + HO.dec_cls_dir
	hashdir =  JV.oDir + JV.sdrHASH
	outputdir = JV.oDir + JV.sdrPART+ HO.dec_cls_dir

	numCLS = load_data(inputdir + 'nbrClusters.txt',fl='nbr') 
	print 'numCLS ',numCLS

	soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
	nbrf = numCLS + 100
	if soft < nbrf:
		resource.setrlimit(resource.RLIMIT_NOFILE, (nbrf, hard))

	print 'start',fr,'job',str(datetime.now())
	CHN = [0] * numCLS
	NBR = [0] * numCLS
	flName = {}
	flDesc = {}

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_F:
			break

		file_name_full = FASTQ[ind]
		file_name = get_file_name(file_name_full)
		file_name_short = cut_sufix_from_name(file_name,sfx='.fastq')
		sample_name = get_prefix_from_name(file_name_short)
		
		sample_file_class = inputdir + 'SMP/' + sample_name +'/' + file_name_short + '.cls'

		clsIND = load_data(sample_file_class,fl='list')
		if type(clsIND[0]) is not list:
			clsIND = [clsIND]

		MB={}
		for cls in range(len(clsIND)):
			for v in clsIND[cls]:
				MB[v] = cls

		f = open_file(FASTQ[ind])
		fGEN = HO.read_from_fastq(f,paired_reads=HO.paired_reads)
		ri = 0
		for fastq_read in fGEN:
			cls = MB[ri]
			if not (cls in flName):
				if not os.path.isfile(outputdir + str(cls) + '/' +  'job.' + str(fr) + '.chn.' + str(CHN[cls]) + '.fastq.gz'):
					flName[cls] = outputdir + str(cls) + '/' +  'job.' + str(fr) + '.chn.' + str(CHN[cls]) + '.fastq'
					flDesc[cls] = open_file(flName[cls],'w')

			if cls in flDesc:
				flDesc[cls].write(fastq_read)
			NBR[cls] += 1

			if NBR[cls] == HO.fReads:
				if cls in flDesc:
					flDesc[cls].close()
					with open_file(flName[cls], 'rb') as f_in, open_file(flName[cls] + '.gz', 'wb') as f_out:
						shutil.copyfileobj(f_in, f_out)
					remove_file(flName[cls])
					del flDesc[cls]
					del flName[cls]

				NBR[cls] = 0
				CHN[cls] += 1
			ri += 1
		f.close()
		print 'done for ',sample_file_class
		print '---------------------------------------'

	for cls in flName.keys():
		flDesc[cls].close()
		with open_file(flName[cls], 'rb') as f_in, open_file(flName[cls] + '.gz', 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)
		remove_file(flName[cls])
		del flDesc[cls]
		del flName[cls]
