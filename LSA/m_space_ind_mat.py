#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from collections import defaultdict
from space_m import indx_2div

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir
	X = [load_data(read_dir + 'X_'+str(i),fl='mm',dump=np.float32) for i in range(HO.nbr_T)]
	nbr = HO.szIndMat()[0]
	print 'nbr',nbr

	for k in range(step):
		ind = step*fr + k
		if ind >= HO.nbr_C:
			break
		print 'k,ind,step',k,ind,step
		strC = ind*HO.cMax
		szC = min(HO.cMax,HO.nbr_H-strC)
		print 'strC,szC',strC,szC
		tIND = np.zeros((nbr,szC), dtype = np.uint64)

		genI = indx_2div(X,nbr,strC,szC)
		for j in range(szC):
			print j,szC,'---------------------'
			tI = genI.next()
			tIND[:,j] = tI[:nbr]
		save_data(tIND,read_dir+'tIND/'+str(ind)+'.npy', fl='npy')


	print 'job - ',fr

	HO.close_AM_files()		
