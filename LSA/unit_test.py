#!/usr/bin/env python

import sys,getopt,os
from scipy.spatial import distance
import numpy as np
import math,time
from lsa_math import *
from lsa_job import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

help_message = "usage example: python define_files.py -o /project/home/"

def get_node_fasta(p2f):

	node_ACGT = ''
	with open(p2f, 'r') as myfile:
		for line in myfile:
			line  = line.split()[0]
			
			if 'NODE' in  line:
				if len(node_ACGT) > 0:
					yield node_name,node_ACGT
					node_ACGT = ''
				node_name = line
			else:
				node_ACGT += line.upper()
		if len(node_ACGT) > 0:
			yield node_name,node_ACGT

def filter_fasta_nodes(path2file):

	fName = get_file_name(path2file)
	dName = get_dir_name(path2file)

	bnd_len = [1000, 3000]
	bnd_cov = [-1, -1]
	dirBounds = ('scaffolds_len_' + str(bnd_len[0]) + '_' + str(bnd_len[1]) + '_cov_'+ str(bnd_cov[0]) + '_' + str(bnd_cov[0]) + '/')
	new_file_name = dName + dirBounds + fName

	f = open_file(new_file_name, 'w')

	for node_name,node_ACGT in get_node_fasta(path2file):
		node_name_blocks = node_name.split('_')
		node_len = int(node_name_blocks[3])
		node_cov = float(node_name_blocks[5])
		flg = True
		if (bnd_len[0] > 0) and (node_len<bnd_len[0]):
			flg = False
		if (bnd_len[1] > 0) and (node_len>bnd_len[1]):
			flg = False
		if (bnd_cov[0] > 0) and (node_cov<bnd_cov[0]):
			flg = False
		if (bnd_cov[1] > 0) and (node_cov>bnd_cov[1]):
			flg = False
		
		if flg:
			f.write(node_name +'\n')
			f.write(node_ACGT +'\n')
	f.close()
	return new_file_name

def ind2sub_cube(ndx,o,d):
	S = np.zeros((o,ndx.size),dtype=np.uint64)
	for i in range(o)[::-1]:
		k = d**i
		vi = np.fmod(ndx, k)
		vj = (ndx - vi)/k
		S[i,:] = vj
		ndx = vi
	return S

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	'''
	o = 3
	d = 2
	ndx = np.array(range(d**o),dtype=np.uint64)
	print ndx
	S= ind2sub_cube(ndx,o,d)
	print S
	'''
	'''
	from scipy.spatial import Delaunay
	import matplotlib.pyplot as plt

	points = np.random.rand(100,2)
	tri = Delaunay(points)
	tri.vertex_neighbor_vertices
	NV=tri.vertex_neighbor_vertices
	for i in range(len(NV[0])-1):
		print NV[1][NV[0][i]:NV[0][i+1]]
	
	plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
	plt.plot(points[:,0], points[:,1], 'o')
	plt.show()
	'''

	'''
	pth = "/env/cns/proj/ADAM/LLDeep/LSA/LSA_hyper_k30_h30/partitions/power.svd_1135_CosineMeans_thr_0.4/spades_prt_"
	#"/env/cns/proj/ADAM/LLDeep/LSA/LSA_hyper_k30_h30/partitions/power.svd_1135_CosineMeans_thr_0.4/spades_prt_0/scaffolds.fasta"
	for i in range(1000):
		path2file = pth + str(i)+'/scaffolds.fasta'
		if os.path.isfile(path2file):
			print i
			filter_fasta_nodes(path2file)
	'''
	from collections import defaultdict

	pth = "/env/cns/proj/ADAM/LLDeep/LSA/LSA_hyper_k30_h30/partitions/IDF_no_st_vdec_1000_IndMax/"


	LD = load_data(pth + 'ld_rdp/ld_rdp_fixrank',fl='str')
	ND = {}
	DC = {}

	PR = defaultdict(list)
	PRn = defaultdict(int)
	print len(LD)
	max_pr = 80
	BN = defaultdict(int)
	nbr_prt = 1001
	for el in LD:
		sp = el.split(';')
		ls = [sp[i].strip('"')+','+sp[i+1] for i in range(len(sp)-2,len(sp),2) if int(sp[i+1][:sp[i+1].find('%')]) >= max_pr]
		if ls:
			ND[sp[0].strip()] = ls
		for v in ls:
			BN[v[:v.find(',')]] += 1
	print len(BN)
	BNL = BN.keys()
	save_data(BNL,pth + 'ld_rdp/bact_'+str(max_pr)+'.names',fl='str')

	print len(BN)

	for i in range(nbr_prt):
		path2file = pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/scaffolds.node'
		if os.path.isfile(path2file):
			ndList = load_data(path2file,fl='str')
			for el in ndList:
				if el in ND:
					if el in DC:
						print el ,DC[el],i
					DC[el] = i

	print len(DC)
	print len(ND)
	print max(DC.values())
	MX = np.zeros((nbr_prt,len(BN)),dtype=np.uint64)
	
	for key,val in ND.iteritems():
		PR[DC[key]].extend(val)
		PRn[DC[key]] += 1

		for el in val:
			nm = el[:el.find(',')]
			ind = BNL.index(nm)
			MX[DC[key],ind] += 1
	
	print len(PR)
	print sorted(PRn.values())
	R = ['#prt,#bac,sum_b,entrp,size,comp,cont,names']
	thr_ckm = [-1,-1]
	for i in range(nbr_prt):
		path2checkm = pth + 'checkm_' + str(i) +'.out'
		if not os.path.isfile(path2checkm):
			print 'no file',path2checkm
			continue
		path2scaf = pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/scaffolds.fasta'
		if not os.path.isfile(path2scaf):
			print 'no file',path2scaf
			continue

		out = os.popen('cat ' + path2checkm + ' | grep N\/A').read().split()
		comp = str2val(out[-3])
		cont = str2val(out[-2])

		if thr_ckm[0] != -1:
			if comp < thr_ckm[0]:
				print i,'comp',comp,cont,thr_ckm
				continue
		if thr_ckm[1] != -1:
			if cont > thr_ckm[1]:
				print i,'cont',comp,cont,thr_ckm
				continue
			
	
		nzi = np.nonzero(MX[i,:])[0]
		vec = MX[i,nzi].astype(np.float)
		sm = int(np.sum(vec))

		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0


		rc =[i,nbs,sm,entrp,file_size(path2scaf),comp,cont,';'.join([BNL[k] for k in nzi])]
		print rc
		R.append(','.join(map(str,rc)))
	save_data(R,pth + 'ld_rdp/parts2bacs_'+str(max_pr)+'_comp_'+str(thr_ckm[0])+'_cont_'+str(thr_ckm[1])+'.stat',fl='str')


	R = ['#bac,name,nbs,sum_p,entrp']
	for j in range(len(BNL)):
		nzi = np.nonzero(MX[:,j])[0]
		vec = MX[nzi,j].astype(np.float)
		sm = int(np.sum(vec))

		vec /= vec.sum()
		nbs = vec.size
		entrp = -sum(vec*np.log(vec)/np.log(nbs)) if nbs > 1 else 0.0
		rc =[j,BNL[j],nbs,sm,entrp]
		R.append(','.join(map(str,rc)))
	save_data(R,pth + 'ld_rdp/bacs2parts_'+str(max_pr)+'.stat',fl='str')

	R = []
	for i in range(nbr_prt):
		R.append(','.join(map(str,MX[i,:])))

	save_data(R,pth + 'ld_rdp/parts2bacs_'+str(max_pr)+'.table',fl='str')
	

	'''
	pv_thr = 0.01
	#pth = "/env/cns/proj/ADAM/LLDeep/LSA/LSA_hyper_k30_h30/partitions/power.svd_1135_CosineMeans_thr_0.4/"
	pth = "/env/cns/proj/ADAM/LLDeep/LSA/LSA_hyper_k30_h30/partitions/IDF_no_st_vdec_1000_IndMax/"
	gr_f = open_file(pth + 'VirFinder_p_val_' + str(pv_thr)+'_scaffolds_len_1000_-1_cov_-1_-1' + '.csv', 'w')
	for i in range(1000):
		path2file = pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/VirFinder.csv'
		path2checkm = pth + 'checkm_' + str(i) +'.out'
		if os.path.isfile(path2file) and  os.path.isfile(path2checkm):
			print i,path2file
			#filter_fasta_nodes(path2file)
			f = open_file(pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/VirFinder_p_val_' + str(pv_thr) + '.csv', 'w')
			j = 0
			nbr = 0
			tnbr = 0
			nodeLS = []
			with open(path2file, 'r') as myfile:
				for read_line in myfile:
					line  = read_line.strip()
					line  = line.split(',')
					if j == 0:
						f.write(read_line)
					else:
						tnbr += 1
						pval = float(line[4])
						if pval < pv_thr:
							nodeLS.append(int(line[0][1:-1]))
							nbr += 1
							f.write(read_line)
							#print line
							#print pval
							
					j += 1
			f.close()

			f_fasta = pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/scaffolds.fasta'

			f_w = open_file(pth + 'spades_prt_' + str(i)+'/scaffolds_len_1000_-1_cov_-1_-1/scaffolds_p_val_' + str(pv_thr) + '.fasta', 'w')

			for node_name,node_ACGT in get_node_fasta(f_fasta):
				node_name_blocks = node_name.split('_')
				node_int = int(node_name_blocks[1])
				if node_int in nodeLS:
					f_w.write(node_name +'\n')
					f_w.write(node_ACGT +'\n')
			
			f_w.close()



			out = os.popen('cat ' + path2checkm + ' | grep N\/A').read().split()
			#print out
			comp = out[-3]
			cont = out[-2]
			out = 'prt,'+str(i)+',t#,'+str(tnbr)+',#p<'+str(pv_thr)+','+str(nbr)+',rt%,'+str(100*float(nbr)/tnbr)[:6]+',comp,'+comp+',cont,'+cont+'\n'
			gr_f.write(out)
			#print out
			#print nodeLS

		#sys.exit()
	gr_f.close()
	'''
