#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml

def merge_LS2file(LS,p2file):
	ff = open_file(p2file,'w')
	cn = 0
	for ls in LS:
		tf = open_file(ls)
		ln = 'dummy'
		while ln:
			ln = tf.readline()
			if ln:
				cn += 1
				ff.write(ln)
		tf.close()
		
	ff.close()
	return cn
		
help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	SL = load_data(JV.oDir + JV.sdrJOB + '/' + 'sampleList.txt',fl='str')#[:20]

	PIDS = []

	for smp in SL:

		p2res = JV.oDir + JV.sdrSAMPLES + 'metap/' + smp + '/'
		
		###################
		LS = get_file_list(PATH = JV.iDir + smp + '/', END = '*fastq*')
		p2fastq = p2res + 'merged.fastq'
		nbr_reads = merge_LS2file(LS,p2fastq)
		nbr_reads /= 4
		save_data(nbr_reads,p2res + 'merged_reads.nbr',fl='nbr')

		JV.jobID = 'MetaP_smp_smpID'
		sname,oname,ename = JV.make_slurm_job()
		sname,oname,ename = JV.convert_slurm_job(sname,oname,ename,smp,pref='smp')

		file_text = load_data(sname,fl='str')
		file_text = rep_in_str(file_text,{'p2fastq':p2fastq})
		save_data(file_text,sname,fl='str')
		print sname
		print '--------------------'
		pid = run_slurm_job(sname)
		PIDS.append(pid)

		PIDS = check_slurm_pid(PIDS)
		while len(PIDS) > 16:
			print time.ctime(), 'MetaP job is running, wait ' + str(10*int(JV.wts)) + ' s'
			time.sleep(10*int(JV.wts))
			PIDS = check_slurm_pid(PIDS)

	wait_for_slurm_jobs(PIDS,int(JV.wts),job = 'MetaP')
