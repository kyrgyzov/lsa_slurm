#!/usr/bin/env python

import sys,getopt,os
from scipy.spatial import distance
import numpy as np
import math,time
from lsa_math import *
from lsa_job import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#from lsa_names import get_file_name,get_dir_name,report_spades_dir

help_message = "usage example: python define_files.py -o /project/home/"

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pth = arg


	from collections import defaultdict

	#pth = "/env/cns/bigtmp1/okyrgyzo/CAMI_download/Gastrointestinal/short_read/"
	FL = get_file_list(PATH = pth, PFX = '*', END = 'reads_mapping.tsv')
	FL.sort()
	print FL

	DCL = [defaultdict(int) for i in range(len(FL))]
	SL = [-1 for i in range(len(FL))]
	
	for i in range(len(FL)):
		fl = FL[i]
		#print fl
		sp = fl.rfind('sample_')
		pp = fl.rfind('/reads/')
		SL[i] = int(fl[sp+7:pp])

	SL.sort()
	print SL	

	for fl in FL:
		print fl
		i = 0 
		with open(fl, 'r') as f:
			for line in f:
				if i % 2:
					ln = line.split()
					sp = ln[0].rfind('S')
					rp = ln[0].rfind('R')
					slp = ln[0].rfind('/')
					sm = int(ln[0][(sp+1):rp])
					rd = int(ln[0][(rp+1):slp])
					smp = SL.index(sm)
					DCL[smp][rd]=int(ln[2])
				i += 1

	gnames = []
	for dc in DCL:
		tk = list(set(dc.values()))
		gnames.extend(tk)
	gnames = list(set(gnames))
	gnames.sort()
	print gnames

	M = np.zeros((len(FL),len(gnames)),dtype=np.uint64)
	for fl in FL:
		print fl
		sp = fl.rfind('sample_')
		pp = fl.rfind('/reads/')

		smp = SL.index(int(fl[sp+7:pp]))
		print smp

		for el in DCL[smp].iteritems():
			ind = gnames.index(el[1])
			M[smp,ind]+=1
	print M

	I = ['s\g'+','+','.join(map(str,gnames))]
	for i in range(len(FL)):
		I.append(FL[i][(FL[i].rfind('/')+1):]+','+','.join(map(str,M[i,:])))
	print I
	save_data(I,pth+'samples2genomes_dist.txt',fl='str')
	save_data(gnames,pth+'genomes.list',fl='list')
	save_data(SL,pth+'samples.list',fl='list')
	save_data(DCL,pth+'DC.list',fl='list',dump='pk')
