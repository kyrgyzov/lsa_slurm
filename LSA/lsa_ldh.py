import numpy as np
import sys

def ldh_check(M,m,c):

    sm = np.sum(M,axis=1)
    t = np.sum(M[:,0])
    next = True
    if m is not None:
        if sum(sm<m) < t:
            next = False
    if c is not None:
        if M.shape[1] == c:
            next = False
    return next

def ldh_vec(M):
    k = M.shape[0]
    t = np.sum(M[:,0])

    sm = np.sum(M,axis=1)
    mx = np.max(sm)

    pr = 1.0 + np.array(mx-sm,dtype=np.float32)
    pr /= np.sum(pr)

    tp = np.random.choice(k, t, replace=False,p=pr)
    tv = np.zeros(k,dtype=np.bool)
    tv[tp] = True

    return tv

def ldh_mat(k, t, c=None):
    
    if c is None:
        c = k

    M = np.zeros((k,c),dtype=np.bool)
    tp = np.random.choice(k, t, replace=False)
    M[tp,0] = True

    for j in range(1,c):
        vc = ldh_vec(M)
        M[:,j] = vc

    NZ = np.zeros((t,c),dtype=np.uint16)
    for j in range(M.shape[1]):
        NZ[:,j] = np.nonzero(M[:,j])[0]

    return NZ
