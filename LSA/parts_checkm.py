#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml

from lsa_names import get_file_name,get_dir_name
from lsa_stats import filter_fasta_nodes,filter_fasta_nodes_name

def combine_spades_checkm_part_stat(JV):

	fRes = JV.oDir + JV.sdrREP + 'spades_checkm/'+JV.dec_cls_dir()  + 'spades_checkm_' + JV.filtered_scaffolds_dir()[:-1] + '.txt'

	print fRes

	f = open_file(fRes, 'w')
	f.write('prt, p_#_reads, p_#_ncl, s_#_nodes, s_#_ncl, s_N50, s_L50, #_g, #_m, #_m_s, 0, 1, 2, 3, 4, 5+, Compl, Contam, S_homog\n')
	
	MAS = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir() + 'nbrClusters.txt',fl='nbr') 

	for i in range(MAS):
		JV.prtID = str(i)
		LS = JV.report_spades_dir() + JV.filtered_scaffolds_dir()
		print LS
		if not os.path.isdir(LS):
			continue

		print '--------------------------------'
		print i,', current partition - ',LS
		path2spades = LS + 'part_stat.txt'
		path2checkm = JV.p2prt + 'checkm_prt_' + str(i) + '/checkm.log'

		print path2spades
		print path2checkm

		empty = False
		if (not os.path.isfile(path2spades)) or (not os.path.isfile(path2checkm)):
			empty =True
		elif (os.stat(path2spades).st_size == 0) or (os.stat(path2checkm).st_size == 0):
			empty =True
		if empty:
			print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
			print path2spades
			print path2checkm
			print 'some files do not exist in'
			continue

		with open_file(path2spades,'r') as tmp:
			spades = tmp.read().splitlines()
		checkm = os.popen('cat '+ path2checkm +' | grep "scaffolds" | tail -1').read().split()

		string2write = JV.prtID
		string2write += ''.join([', '+val.split()[-1] for val in spades])
		string2write += ', '
		string2write += ', '.join(checkm[3:])
		string2write += '\n'
		f.write(string2write)

	f.close()


help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	MAS = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir() + 'nbrClusters.txt',fl='nbr') 

	PIDS = []

	for prt in range(MAS):
		print prt,'------------------------------'
		p2spades = JV.p2prt + 'spades_prt_' + str(prt) + '/'
		print p2spades
		remove_dir(p2spades + 'bins')

		p2scaffolds = filter_fasta_nodes_name(p2spades + 'scaffolds.fasta',JV)
		if file_size(p2scaffolds) <= 0:
			if file_size(p2spades + 'scaffolds.fasta') <= 0:
				continue
			else:
				p2scaffolds = filter_fasta_nodes(p2spades + 'scaffolds.fasta',JV)

		if file_size(p2scaffolds) <= 0:
			continue

		print p2scaffolds

		os.mkdir(p2spades + 'bins')
		os.symlink(p2scaffolds,p2spades + 'bins/scaffolds.fasta')

		JV.jobID = 'Checkm_prt_prtID'
		sname,oname,ename = JV.make_slurm_job()
		sname,oname,ename = JV.convert_slurm_job(sname,oname,ename,prt,pref='prt')

		sys.exit()
		pid = run_slurm_job(sname)
		PIDS.append(pid)
		break

	wait_for_slurm_jobs(PIDS,int(JV.wts),job = 'Checkm')

	combine_spades_checkm_part_stat(JV)
