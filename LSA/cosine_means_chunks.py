#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	savedir = JV.oDir + JV.sdrCLS + HO.dec_cls_dir
	path2cls = savedir + 'CLS/'

	tCentroids = np.zeros((0,HO.nbr_T))
	tCounts = np.zeros(0,dtype=np.uint64)
	if os.path.isfile(savedir + 'cluster_centroids.npy') and os.path.isfile(savedir + 'cluster_counts.npy'):
		tCentroids = load_data(savedir + 'cluster_centroids.npy',fl='npy')#np.zeros((0,self.nbr_T))
		tCounts = load_data(savedir + 'cluster_counts.npy',fl='npy')# np.zeros(0,dtype=np.uint64)

	prm = load_data(path2cls + 'prm.npy',fl='npy')

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_C:
			break
		
		tCentroids,tCounts =  HO.adjust_cluster_centroids(model,tCentroids,tCounts,prm[ind])
		break


	print 'job - ',fr,', cls -', tCounts.shape[0]

	fname = path2cls + 'CO.' + str(fr) + '.npy'
	save_data(tCounts,fname,fl='npy')

	fname = path2cls + 'CE.' + str(fr) + '.npy'
	save_data(tCentroids,fname,fl='npy')

	HO.close_AM_files()		
