#!/usr/bin/env python

import sys,getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from gensim import models
import time
from multiprocessing import Process, Manager
from scipy.spatial import distance
import math
from job_vars import JobVars
from lsa_math import safe_cosine
from numpy import linalg as la

help_message = 'usage example: python kmer_vectors.py -p /project/home/ -n 8 -d'
if __name__ == "__main__":

	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('vectors')
	hashobject = StreamingEigenhashes(JV.oDir)

	model= hashobject.load_decomp_model()
	save_dir = JV.oDir + JV.sdrCLS



	for i in range(step):
		ind = step*fr + i
		print 'ind',ind,hashobject.nbr_T
		if ind >= hashobject.nbr_T:
			break

                V = []
                for k in xrange(hashobject.nbr_C):
                    vectors = load_data(JV.oDir + JV.sdrCLS + 'VEC/' + extend_path(str(k)) + '.v',fl='list')
                    for doc in vectors:
                        V.append(doc[ind])

                
                n = len(V)
                print ind,'-',n
                
                INDX = sorted(range(n), key=lambda k: V[k])
                INDXI= sorted(range(n), key=lambda k: INDX[k])

		fName = save_dir + 'INDX/' + str(ind) + '.i'
		save_data(INDX,fName ,fl='list')
		fName = save_dir + 'INDXI/' + str(ind) + '.ii'
		save_data(INDXI,fName,fl='list')
