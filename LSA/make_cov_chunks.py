#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
import  scipy.optimize as sop

help_message = 'usage example: python make_corpus_chunks.py -r 0 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	path2cov = HO.oDir + HO.sdrTMP + 'COV/'

	T = load_data(HO.oDir + HO.sdrCLS + 'T.npy',fl='npy')
	ln = T.shape[0]
	COV = np.zeros((ln,ln))

	HO.open_AM_files()
	nonneg = 'nmf' in HO.decType

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_C:
			break

		print 'i---------------------------',i
		gt = time.time()

		t = time.time()
		M =  HO.get_full_hash_chunk(ind)
		print 'M was created', M.shape,T.shape,time.time() - t

		R = np.dot(T,M)

		if nonneg:
			R[R<0] = 0.0


		t = time.time()
		COV += np.dot(R,R.T)
		print 'COV +=',COV.shape,time.time() - t

		print time.time() - gt

	fname = path2cov + str(fr) + '.npy'
	save_data(COV,fname,fl='npy')
	HO.close_AM_files()		
