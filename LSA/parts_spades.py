#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_job import *
from job_vars import JobVars
import yaml

def merge_LS2file(LS,p2file):
	ff = open_file(p2file,'w')
	cn = 0
	for ls in LS:
		tf = open_file(ls)
		ln = 'dummy'
		while ln:
			ln = tf.readline()
			if ln:
				cn += 1
				ff.write(ln)
		tf.close()
		
	ff.close()
	return cn
		
help_message = 'usage example: python kmer_cluster_merge.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	MAS = load_data(JV.oDir + JV.sdrCLS + JV.dec_cls_dir()  + 'nbrClusters.txt',fl='nbr') 

	PIDS = []

	for prt in range(MAS):
		if not os.path.isdir(JV.p2prt + str(prt) + '/'):
			print 'no ',JV.p2prt + str(prt)
			continue
		p2res = JV.p2prt + 'spades_prt_' + str(prt) + '/'
		print p2res
		
		###################
		if not os.path.isfile(p2res + 'file_list.yaml') or not os.path.isfile(p2res + 'merged.fastq'):
			LS = get_file_list(PATH = JV.p2prt + str(prt) + '/',PFX = 'job' , END = '*gz')
			p2fastq = p2res + 'merged.fastq'
			nbr_reads = merge_LS2file(LS,p2fastq)
			nbr_reads /= 4
			save_data(nbr_reads,p2res + 'merged_reads.nbr',fl='nbr')
			#print LS
			yaml_data = dict()

			if JV.paired_reads == 'True':
				yaml_data['type'] = 'paired-end'
				yaml_data['interlaced reads'] = [p2fastq]
			else:
				yaml_data['type'] = 'single'
				yaml_data['single reads'] = [p2fastq]
			yaml_data['orientation'] = 'fr'

			yaml_file = p2res + 'file_list.yaml'
			with open_file(yaml_file, 'w') as outfile:
				yaml.dump([yaml_data], outfile, default_flow_style=True)

		###################

		JV.jobID = 'Spades_prt_prtID'
		sname,oname,ename = JV.make_slurm_job()
		sname,oname,ename = JV.convert_slurm_job(sname,oname,ename,prt,pref='prt')
		print sname
		print '--------------------'
		pid = run_slurm_job(sname)
		PIDS.append(pid)
	wait_for_slurm_jobs(PIDS,int(JV.wts),job = 'Spades')
