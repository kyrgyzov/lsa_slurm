#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from lsa_job import *
from lsa_stats import *
from job_vars import JobVars

help_message = 'usage example: python kmer_cluster_centroids.py -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:r:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg
		elif opt in ('-r'):
			fr = int(arg)

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('clusters')

	savedir = JV.oDir + JV.sdrCLS + JV.dec_cls_dir()
	MAS = load_data(savedir + '/nbrClusters.txt',fl='nbr') 

	for i in range(step):
		ind = step*fr + i

		if ind >= MAS:
			break

		cami_part_dist(JV,ind)
