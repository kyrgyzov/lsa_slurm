#!/usr/bin/env python

import sys, getopt
import glob, os,time
import numpy as np
from lsa_names import get_file_name,get_dir_name

from lsa_job import *
from lsa_stats import filter_fasta_nodes,get_node_fasta
from job_vars import JobVars

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def get_N50_stat(LS):
	if len(LS) == 0:
		return 0,0
	LS.sort()
	hsm = sum(LS)/2
	i = 0
	csm = 0
	while csm < hsm:
		csm += LS[i]
		i += 1
	i -= 1
	N50 = LS[i]
	L50 = min(i,len(LS)-i)
	return N50,L50


def part_stat(p2res,p2spades,p2scaffolds):
	f = open_file(p2res+'part_stat.txt', 'w')

	part_nbr_reads = int(load_data(p2spades + 'merged_reads.nbr',fl='nbr'))
	part_nbr_ncl = int(JV.nbr_NR) * part_nbr_reads
	f.write('part_nbr_reads - '+str(part_nbr_reads)+'\n')
	f.write('part_nbr_ncl - '+str(part_nbr_ncl)+'\n')

	scaffolds_nodes = os.popen('cat '+ p2scaffolds +' | grep NODE').read().split()
	scaffolds_nbr_lines = int(os.popen('wc -l '+ p2scaffolds).read().split()[0])
	scaffolds_nbr_nodes = len(scaffolds_nodes)
	scaffolds_nbr_ncl  = file_size(p2scaffolds) - sum([len(val) for val in scaffolds_nodes]) - scaffolds_nbr_lines
	f.write('scaffolds_nbr_nodes - ' + str(scaffolds_nbr_nodes) + '\n')
	f.write('scaffolds_nbr_ncl - ' + str(scaffolds_nbr_ncl) + '\n')
	scaffolds_nodes_len = [int(val.split("_")[-3]) for val in scaffolds_nodes]
	scaffolds_N50,scaffolds_L50 = get_N50_stat(scaffolds_nodes_len)
	f.write('scaffolds_N50 - '+str(scaffolds_N50)+'\n')
	f.write('scaffolds_L50 - '+str(scaffolds_L50)+'\n')
	f.close()


def cg_ratio(p2scaffolds,p2res):
	if not os.path.isdir(p2res):
		os.mkdir(p2res)
	f = open(p2res+'CG_ratio_cov_len.txt', 'w')
	M=np.zeros((0,3))
	for node_name,node_ACGT in get_node_fasta(p2scaffolds):
		node_CG_ratio = float((node_ACGT.count('C')+node_ACGT.count('G')))/(node_ACGT.count('C')+node_ACGT.count('G')+node_ACGT.count('A')+node_ACGT.count('T'))
		node_name = node_name.split('_')
		node_len = int(node_name[-3])
		node_cov = float(node_name[-1])
		f.write(str(node_CG_ratio) +',' + str(node_cov) + ',' + str(node_len)+'\n')
		M = np.vstack([M,[node_CG_ratio,node_cov,node_len]])
	f.close()
	sind =np.argsort(M[:,-1])
	M = M[sind,:]

	n = 50
	cm = plt.cm.get_cmap('jet')
	fig, ax = plt.subplots()

	mn = min(M[:,2])
	mx = max(M[:,2])
	if mx == mn: mn = 0

	colorIndex = (M[:,2]-mn)/(mx-mn)

	if M.shape[0] > 0:
		colorIndex = (M[:,2]-mn)/(mx-mn)

	sc = ax.scatter(M[:,0],M[:,1],s=M[:,2]/10.0,c=colorIndex,cmap=cm,linewidth=1,alpha=0.5)
	ax.grid()
	ax.set_xlim([0.1,0.5])
	ax.set_xlabel('contig GC %')
	ax.set_ylim([0.0,M[:,1].max()*1.1])
	ax.set_ylabel('contig coverage')


	fig.savefig(p2res+'CG_ratio_cov_len.pdf',format='pdf')
	plt.close(fig)

help_message = 'usage example: python script_name.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hp:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg

        '''
	JV = JobVars(pDir)
	JV.prtID = prt
	p2spades = JV.p2prt + 'spades_prt_' + JV.prtID + '/'
	p2scaffolds = p2spades + 'scaffolds.fasta'

	print 'current partition - ',p2spades
	if file_size(p2scaffolds) <= 0:
		print p2scaffolds + '\ndoes not exist. Exit.'
		sys.exit()

	#print 'filter_fasta_nodes'
	p2scaffolds = filter_fasta_nodes(p2scaffolds,JV)

	if file_size(p2scaffolds) <= 0:
		print p2scaffolds + '\ndoes not exist. Exit.'
		sys.exit()
	p2res = JV.report_spades_dir() +  JV.filtered_scaffolds_dir()
	'''

	FASTAS = glob.glob(pDir + '/*.fasta')
	print FASTAS
	for fl in FASTAS:
		p2res = fl[:fl.rfind('.')] + '/'
		print fl,p2res
		cg_ratio(fl,p2res)
