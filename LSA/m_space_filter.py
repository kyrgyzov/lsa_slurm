#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars
from collections import defaultdict
from space_m import  iter_indx_direct,uv_un_D

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	read_dir = HO.oDir + HO.sdrTMP + HO.dec_cls_dir
	X = [load_data(read_dir + 'X_'+str(i),fl='mm',dump=np.float32) for i in range(HO.nbr_T)]
	VI = np.memmap(read_dir+'VI', dtype = np.uint64, mode='r+', shape=(HO.nbr_H,))
	IND = np.memmap(read_dir+'IND', dtype = np.uint64, mode='r', shape=HO.szIndMat())

	mnb = HO.ms_m()

	for k in range(step):
		ind = step*fr + k
		if ind >= HO.nbr_C:
			break

		strC = ind*HO.cMax
		szC = min(HO.cMax,HO.nbr_H-strC)
		tVI=np.zeros(szC,dtype=np.uint64)

		for j in xrange(szC):
			tI,tV =  iter_indx_direct(X,IND,mnb,strC+j)
			if np.sum(VI[strC+j] != VI[tI]):
				tD = defaultdict(int)
				for v in VI[tI]:
					tD[v]+=1
				ky = tD.keys()
				va = tD.values()
				nv = ky[np.argmax(va)]

				tVI[j] = nv
		save_data(tVI,read_dir+'tFI/'+str(ind)+'.npy',fl='npy')

	print 'job - ',fr

	del VI,IND
	HO.close_AM_files()		
