#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
from streaming_eigenhashes import StreamingEigenhashes
from lsa_job import *
from job_vars import JobVars

help_message = 'usage example: python script.py -r 0 -p /project/home/'

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)
	for opt, arg in opts:
		if opt in '-h':
			print help_message
			sys.exit()
		elif opt in ('-r'):
			fr = int(arg)
		elif opt in ('-p'):
			pDir = arg

        JV = JobVars(pDir)
	AS,step = JV.get_array_size('chunks')

	HO = StreamingEigenhashes(JV.oDir)
	model = HO.load_decomp_model()

	HO.open_AM_files()

	savedir = JV.oDir + JV.sdrTMP + HO.dec_cls_dir + 'TR/'

	for i in range(step):
		ind = step*fr + i
		if ind >= HO.nbr_C:
			break

		hash_gen = HO.get_gen_hash_chunk(ind)
		all_vectors = HO.transform_vectors(model,hash_gen)
		D = np.zeros((HO.nbr_T,HO.cMax),dtype = np.float32)
		j = 0
		for col in all_vectors:
			D[:,j] = col
			j += 1
		D = D[:,:j]
		print i,ind,D.shape
		ext_p = extend_path(str(ind)) + '/'
		for k,line in enumerate(D):
			save_data(D[k,:],savedir + ext_p +  str(k) + '.npy',fl='npy')



	print 'job - ',fr

	HO.close_AM_files()		
