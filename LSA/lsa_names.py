import string
import random

def id_generator(size = 10, chars=string.letters + string.digits):
      return ''.join(random.choice(chars) for _ in range(size))

def get_file_name(FP):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[f.rfind('/')+1:] for f in FP]
      if fl:
            RS=RS[0]
      return RS

def get_dir_name(FP):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[:f.rfind('/')+1] for f in FP]
      if fl:
            RS=RS[0]
      return RS

def get_prefix_from_name(FP):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[:f.index('.')] for f in FP]
      if fl:
            RS=RS[0]
      return RS
def cut_prefix_from_name(FP):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[f.index('.')+1:] for f in FP]
      if fl:
            RS=RS[0]
      return RS

def get_sufix_from_name(FP):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[f.rfind('.')+1:] for f in FP]
      if fl:
            RS=RS[0]
      return RS

def cut_sufix_from_name(FP,sfx='.'):
      fl=False
      if type(FP) is str:
            fl = True
            FP = [FP]
      RS=[f[:f.rfind(sfx)] for f in FP]
      if fl:
            RS=RS[0]
      return RS

def file_name_merged(outputdir,sname,fname,ch):
    fnm = outputdir+ '/' + sname + '/' +  fname +'.'+ 'CH' + ch +  '.fastq.gz'
    return fnm
