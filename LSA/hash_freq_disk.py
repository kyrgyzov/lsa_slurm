#!/usr/bin/env python

import glob,os
import sys, getopt
import gzip,time
from fastq_reader import Fastq_Reader
from lsa_job import *
from lsa_math import kmer_bins
from job_vars import JobVars
from collections import defaultdict
from scipy.sparse import dok_matrix,vstack

help_message = 'usage example: python hash_fastq_reads.py -r 1 -p /project/home/'
if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:],'hr:p:')
	except:
		print help_message
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h'):
			print help_message
			sys.exit()
		elif opt in ('-p'):
			pDir = arg


        JV = JobVars(pDir)
	FP = load_data(JV.oDir + JV.sdrJOB + '/fileList.txt',fl='str')

	AS,step = JV.get_array_size('files')
	HO = Fastq_Reader(JV.oDir)

	rHV = load_data(JV.oDir + JV.sdrCLS + 'nbr_hashes',fl='mm',dump=np.uint32)
	print 'sum(rHV)',np.sum(rHV),rHV.size
	sVC = np.sum(rHV)

	CS = np.cumsum(np.hstack((0,rHV)))
	save_data(CS,HO.oDir + HO.sdrCLS + 'CS.cs',fl='mm')

	IM = np.memmap(HO.oDir + HO.sdrCLS + 'IM.im', dtype = np.uint64, mode='w+', shape=(sVC,))
