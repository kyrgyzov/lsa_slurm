# Project name: Metagenomic read binning using sparse coding
# Operating system(s): Linux
# Programming language: Python, version 2.x

# The Main Info:

We refer the user to check Brian P. Cleary's codes `https://github.com/brian-cleary/LatentStrainAnalysis`
and reports `https://latentstrainanalysis.readthedocs.io/en/latest/getting_started.html`  to get the initial base for this job.

The code was cloned from
https://github.com/brian-cleary/LatentStrainAnalysis
and has been totally modified to the SLURM Workload Manager.

The initial algorithms were modified as well.

# To run a SLURM script in the  BASH:

The root working dir of the LSA pipeline is `LatentStrainAnalysis`:
```sh
$ pwd
/path2LSA/LatentStrainAnalysis

$ ls
data  demo  LICENSE  LSA  misc  PRJ  README.md  requirements.txt  results  SLURM  tmp
```

There is an entry point in the LSA pipeline (`SLURM/create_lsa_jobs.py`), where we have to specify a project name and a job name.
```sh
$ ls SLURM/
create_lsa_jobs.py  default.vals  footer.code  header.code  scripts.code
```

The project files are located in `PRJ/` directory.
```sh
$ ls PRJ/
demo_Spike adam_demo_MergeChunkSamples_Pitch ...
```
Each file specifies a list of the LSA pipeline phases and thier parameters.

To implement a list of jobs by one call we have to make the `Master` job, which is a list `MSR`  into configuration file: 
```sh
$ cat PRJ/demo_Spike | grep MSR
MSR,"SetupProject,MakeHashBase,HashReads,CountHashFiles,CountHashSamples,CompressIndx,CompressHashes,CompressHists,DecomposeMatrix,DefineClusters,ClusterHash,ClassifyHash,ClassStats,BuildFastqParts,SpikeResultStats"
```

For example, to make a project `genoscope_Spike` and run the `Master` script we do:
```sh
$ python SLURM/create_lsa_jobs.py -p genoscope_Spike -j Master
------------------------------------------------------------------
old /env/kyrgyzov/plusSpike/LSA_k33_h16/scripts/Master.slurm.job file was removed
old /env/kyrgyzov/plusSpike/LSA_k33_h16/logs/Master.slurm.out file was removed
old /env/kyrgyzov/plusSpike/LSA_k33_h16/logs/Master.slurm.err file was removed
new Master SLURM job was successfully created in file:
/env/kyrgyzov/plusSpike/LSA_k33_h16/scripts/Master.slurm.job

$ sbatch /env/kyrgyzov/plusSpike/LSA_k33_h16/scripts/Master.slurm.job
```

# An example for real Spike Dataset:

Here, we compare our results with those from `https://latentstrainanalysis.readthedocs.io/en/latest/getting_started.html`.

## Initial test with SVD:
Download the initial Spike dataset from Brian P. Cleary's codes `https://github.com/brian-cleary/LatentStrainAnalysis`:

```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/

$ ll
total 72
drwxrwsr-x 2 okyrgyzo g_adam  4096 Jul 21 05:41 data
drwxrwsr-x 2 okyrgyzo g_adam  4096 Sep  8  2017 demo
-rw-rwxr-x 1 bruls    g_adam  1132 Jul 12  2016 LICENSE
drwxrwsr-x 2 bruls    g_adam 16384 Jul 20 15:23 LSA
drwxrwsr-x 3 bruls    g_adam  4096 Jan 17  2019 misc
drwxrwsr-x 2 bruls    g_adam  8192 Jul 20 15:46 PRJ
-rw-rwxr-x 1 bruls    g_adam 19041 Jul 20 15:31 README.md
-rw-rw-r-- 1 okyrgyzo g_adam    54 Jul 20 00:14 requirements.txt
drwxrwsr-x 2 okyrgyzo g_adam  4096 Jul 20 15:25 results
drwxrwsr-x 2 bruls    g_adam  4096 Jul 20 15:02 SLURM
drwxrwsr-x 2 okyrgyzo g_adam  4096 Jan 14  2019 tmp

$ ls -R data/ results/
data/:

results/:

$ cd  data/
$ mkdir Spikes
$ cd Spikes/
$ wget https://github.com/brian-cleary/LatentStrainAnalysis/raw/master/testData.tar.gz
--2019-07-21 05:51:59--  https://github.com/brian-cleary/LatentStrainAnalysis/raw/master/testData.tar.gz
Resolving github.com... 140.82.118.4
Connecting to github.com|140.82.118.4|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://raw.githubusercontent.com/brian-cleary/LatentStrainAnalysis/master/testData.tar.gz [following]
--2019-07-21 05:51:59--  https://raw.githubusercontent.com/brian-cleary/LatentStrainAnalysis/master/testData.tar.gz
Resolving raw.githubusercontent.com... 151.101.120.133
Connecting to raw.githubusercontent.com|151.101.120.133|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17790844 (17M) [application/octet-stream]
Saving to: “testData.tar.gz”

100%[=============================================================================>] 17,790,844  72.7M/s   in 0.2s

2019-07-21 05:52:01 (72.7 MB/s) - “testData.tar.gz” saved [17790844/17790844]

$ tar -zxf testData.tar.gz --wildcards "*.fastq"

$ ls
original_reads  testData.tar.gz

$ ls original_reads/
SRR492065.10kReads.plusSpike.fastq  SRR492186.10kReads.plusSpike.fastq  SRR492192.10kReads.plusSpike.fastq
SRR492066.10kReads.plusSpike.fastq  SRR492187.10kReads.plusSpike.fastq  SRR492193.10kReads.plusSpike.fastq
SRR492182.10kReads.plusSpike.fastq  SRR492188.10kReads.plusSpike.fastq  SRR492194.10kReads.plusSpike.fastq
SRR492183.10kReads.plusSpike.fastq  SRR492189.10kReads.plusSpike.fastq  SRR492195.10kReads.plusSpike.fastq
SRR492184.10kReads.plusSpike.fastq  SRR492190.10kReads.plusSpike.fastq  SRR492196.10kReads.plusSpike.fastq
SRR492185.10kReads.plusSpike.fastq  SRR492191.10kReads.plusSpike.fastq  SRR492197.10kReads.plusSpike.fastq


```
Now, for jobs balancing,  we need to prepare chunks of the fastq files.
The configuration file for this process is  

```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/

$ cat PRJ/demo_MergeChunkSamples_Spike
cDir,/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/
iDir,/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/data/Spikes/original_reads/
oDir,/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/
sPart,small

p2python,python

cReads,1000
PFX,SRR
MSR,"SetupProject,MergeChunkFastq"
```
We need to note that for any case  you need to change vars `cDir, iDir, oDir, sPart, p2python`, where all `*Dir` vars mean different directories.
We keep all `*Dir` in the same place just to simplify this current example. 

```sh
cDir - code directory (path to LatentStrainAnalysis/ dir)
iDir - input directory (path, where we keep data to process)
oDir - output directory (path, where to save results)
sPart - the name of you partition in SLURM (check sinfo on your machine)
p2python - path to your python interpreter
cReads - the number of reads par chank
PFX - prefix of samples to process
```

Here, we make the `Master` script:

```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis

$ python SLURM/create_lsa_jobs.py -p demo_MergeChunkSamples_Spike -j Master
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_None_qcF_fuT/scripts/Master.slurm.job file was removed
new Master SLURM job was successfully created in file:
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_None_qcF_fuT/scripts/Master.slurm.job

```
Now, let us start SLURM script and check results:

```sh
$ sbatch /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_None_qcF_fuT/scripts/Master.slurm.job
Submitted batch job 10685818

$ squeue | grep kyrg; squeue | grep kyrg | wc -l
           8028381     small SetupPro okyrgyzo PD       0:00      1 (None)
           8028379     small   Master okyrgyzo  R       0:32      1 etna49
2
$ squeue | grep kyrg; squeue | grep kyrg | wc -l
0

$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis

$ ls results/LSA_None_qcF_fuT/samples/
SRR492065  SRR492183  SRR492186  SRR492189  SRR492192  SRR492195
SRR492066  SRR492184  SRR492187  SRR492190  SRR492193  SRR492196
SRR492182  SRR492185  SRR492188  SRR492191  SRR492194  SRR492197

$ ls results/LSA_None_qcF_fuT/samples/SRR492065                               
SRR492065.10kReads.plusSpike.CH0.fastq.gz   SRR492065.10kReads.plusSpike.CH4.fastq.gz
SRR492065.10kReads.plusSpike.CH10.fastq.gz  SRR492065.10kReads.plusSpike.CH5.fastq.gz
SRR492065.10kReads.plusSpike.CH11.fastq.gz  SRR492065.10kReads.plusSpike.CH6.fastq.gz
SRR492065.10kReads.plusSpike.CH1.fastq.gz   SRR492065.10kReads.plusSpike.CH7.fastq.gz
SRR492065.10kReads.plusSpike.CH2.fastq.gz   SRR492065.10kReads.plusSpike.CH8.fastq.gz
SRR492065.10kReads.plusSpike.CH3.fastq.gz   SRR492065.10kReads.plusSpike.CH9.fastq.gz

Move data into a new directory:
```sh
$ mkdir data/Spikes/chunked_reads
$ mv results/LSA_None_qcF_fuT/samples/SRR492*/ data/Spikes/chunked_reads/
$ rm -r results/LSA_None_qcF_fuT/
```

### K-mer clustering with threshold value 0.2

Here, we process the chunked dataset and compare our results with `https://latentstrainanalysis.readthedocs.io/en/latest/getting_started.html` (we keep the same values for all variables):
```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis
```
Agin, we need to change variables vars `cDir, iDir, oDir, sPart, p2python` into PRJ/demo_Spike accordingly to your configuration.

```sh
$ python SLURM/create_lsa_jobs.py -p demo_Spike  -j Master
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job file was removed
new Master SLURM job was successfully created in file:
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job

$ sbatch /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job
Submitted batch job 10686321
```

done?
check results into:

```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF
$ ls
clusters  hashes  job  JobVars  logs  partitions  reports  samples  scripts  tmp

$ ls logs/
BuildFastqParts  ClusterChunks   CompressHists      CountHashFiles    DefineClusters  MakeHashBase     SetupProject
ClassifyHash     ClusterHash     CompressIndx       CountHashSamples  HashReads       MakeHashVectors  SpikeResultStats
ClassStats       CompressHashes  CosineMeansChunks  DecomposeMatrix   MakeCovChunks   Master

$ ls partitions/
IDF_nr2_hw_power.svd_18_CosineMeans_thr_0.2

$ ls reports/
hash  parts  reads2class
``` 

The number of recovered clusters:
```sh

$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF
$ cat clusters/IDF_nr2_hw_power.svd_18_CosineMeans_thr_0.2/nbrClusters.txt
175
```
Distribution of Spikes among clasters:
```sh
$ cat reports/parts/IDF_nr2_hw_power.svd_18_CosineMeans_thr_0.2/stat_prt_* | less
 file name    # of spikes     # of reads          ratio
   0.fastq              0            432       0.000000
 file name    # of spikes     # of reads          ratio
 100.fastq              0            526       0.000000
...
 file name    # of spikes     # of reads          ratio
  34.fastq              0            530       0.000000
 file name    # of spikes     # of reads          ratio
  35.fastq          20374          20380       0.999706
 file name    # of spikes     # of reads          ratio
  36.fastq              0            266       0.000000
...
```
So, all Spikes are into one homogen and complete cluster.

### K-mer clustering with threshold value 0.3


Let us perform another clusteriing test when the threshold equals  0.3.
Into the configuration file `demo_Spike` we change:
```sh
clThresh,0.3
```
For this threshold value, the project starts from the `DefineClusters` procedure, so
```sh
#MSR,"SetupProject,MakeHashBase,HashReads,CountHashFiles,CountHashSamples,CompressIndx,CompressHashes,CompressHists,DecomposeMatrix,DefineClusters,ClusterHash,ClassifyHash,ClassStats,BuildFastqParts,SpikeResultStats"
MSR,"DefineClusters,ClusterHash,ClassifyHash,ClassStats,BuildFastqParts,SpikeResultStats"
```
recompile the project and start the SLURM script:

```sh
$ cd  /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/
$ python SLURM/create_lsa_jobs.py -p demo_Spike  -j Master
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job file was removed
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/logs/Master/ was removed
new Master SLURM job was successfully created in file:
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job

$ sbatch /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job
Submitted batch job 120545
```

done (`squeue` is empty)?  compare results with the previos test:

the number of recovered clusters:
```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF

$ cat clusters/IDF_nr2_hw_power.svd_18_CosineMeans_thr_0.3/nbrClusters.txt
20
```
Distribution of Spikes among clasters:
```sh
$ cat reports/parts/IDF_nr2_hw_power.svd_18_CosineMeans_thr_0.3/stat_prt_*
 file name    # of spikes     # of reads          ratio
   0.fastq          20374          29638       0.687428
 file name    # of spikes     # of reads          ratio
  10.fastq              0           9276       0.000000
...
```
So, all Spikes are into one complete but not homogen cluster.

## Initial test with NMF with 30 components:
Into the configuration file `demo_Spike` we change:
```sh
clsType,IndMax
#clsType,CosineMeans
#clThresh,0.3

#decType,power.svd
decType,spams.nmf
decRank,30
```
and
```sh
#MSR,"SetupProject,MakeHashBase,HashReads,CountHashFiles,CountHashSamples,CompressIndx,CompressHashes,CompressHists,DecomposeMatrix,DefineClusters,ClusterHash,ClassifyHash,ClassStats,BuildFastqParts,SpikeResultStats"
MSR,"DecomposeMatrix,DefineClusters,ClusterHash,ClassifyHash,ClassStats,BuildFastqParts,SpikeResultStats"
```

recompile the project and start the SLURM script:

```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis

$ python SLURM/create_lsa_jobs.py -p demo_genoscope_Spike  -j Master
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job file was removed
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/results/LSA_hyper_k33_h22_qcT_fuF/logs/Master/ was removed
new Master SLURM job was successfully created in file:
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job
$ sbatch /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF/scripts/Master.slurm.job
Submitted batch job 8036410
```

For the current implementation, the project starts from the `DecomposeMatrix` procedure.

done (`squeue` is empty)?  compare results with the previos test:

the number of recovered clusters:
```sh
$ pwd
/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/results/LSA_hyper_k33_h22_qcT_fuF
$ cat clusters/IDF_nr2_hw_spams.nmf_30_IndMax/nbrClusters.txt
30

```
Distribution of Spikes among clasters:
```sh
$ cat reports/parts/IDF_nr2_hw_spams.nmf_30_IndMax/stat_prt_*
 file name    # of spikes     # of reads          ratio
   0.fastq          20374          20642       0.987017
 file name    # of spikes     # of reads          ratio
  10.fastq              0          12476       0.000000
...
```
So, all Spikes are into one complete and homogen cluster.

# An example for real dataset Droplets:

Let us have dataset with fastq files:

```sh
$ pwd
/genoscope2/Data/Droplets

$ ls
PITCH_AEY_AMC PITCH_CAC_AB PITCH_CAC_AF PITCH_CAC_AJ PITCH_CAC_AO PITCH_CAC_AS PITCH_CAC_AW PITCH_CAC_BW PITCH_CAC_CA  PITCH_CAC_CE
PITCH_AEY_AMD PITCH_CAC_AC PITCH_CAC_AG PITCH_CAC_AL PITCH_CAC_AP PITCH_CAC_AT PITCH_CAC_AX PITCH_CAC_BX PITCH_CAC_CB  PITCH_CAC_CF
PITCH_AEY_ML PITCH_CAC_AD PITCH_CAC_AH PITCH_CAC_AM PITCH_CAC_AQ PITCH_CAC_AU PITCH_CAC_AY PITCH_CAC_BY PITCH_CAC_CC  PITCH_CAC_CG
PITCH_CAC_AA PITCH_CAC_AE PITCH_CAC_AI PITCH_CAC_AN PITCH_CAC_AR PITCH_CAC_AV PITCH_CAC_AZ PITCH_CAC_BZ PITCH_CAC_CD  PITCH_CAC_CH

$ ls PITCH_CAC_AA
PITCH_CAC_AA.OSDC_1_1_B3NPK.12BA120_clean.fastq.gz  PITCH_CAC_AA.OSDC_1_2_B3NPK.12BA120_clean.fastq.gz
```
There are 40 fastq data samples: 3 names start with prefix `PITCH_AEY_` and 37 - `PITCH_CAC_`.

Our LSA pipeline implies to process merged and chunked fastq files. 
Data can be unpaired, but if thay are paired, reads have to be in one file.
Data chunking helps us to perform uniform balancing among CPU/cores.
From the above output we see that for each data sample we have paired reads kept in separate files.

Let us merge and chunk paired fastq files for data samples with prefix `PITCH_CAC_`. 

To run merging/chunking:

```sh
$ python SLURM/create_lsa_jobs.py -p adam_demo_MergeChunkSamples_Pitch -j Master
/genoscope2/Results/Droplets/LSA_None_qcT_fuF/scripts/Master.slurm.job file was removed
/genoscope2/Results/Droplets/LSA_None_qcT_fuF/logs/Master/ was removed
new Master SLURM job was successfully created in file:
/genoscope2/Results/Droplets/LSA_None_qcT_fuF/scripts/Master.slurm.job

$ sbatch /genoscope2/Results/Droplets/LSA_None_qcT_fuF/scripts/Master.slurm.job
```
where, the project file `adam_demo_MergeChunkSamples_Pitch` is `$ cat PRJ/adam_demo_MergeChunkSamples_Pitch`:
```sh
cDir,/genoscope2/Code/LatentStrainAnalysis/
iDir,/genoscope2/Data/Droplets/
oDir,/genoscope2/Results/Droplets/

cReads,100000
sPart,adamme
PFX,PITCH_CAC_
MSR,"SetupProject,MergeChunkFastq"
p2python,python
```

When all jobs are over
```sh
$ squeue
    JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
$
```

we can check results in:

```sh
$ cd /genoscope2/Results/Droplets/LSA_None_qcT_fuF/samples/

$ ls
PITCH_CAC_AA PITCH_CAC_AD PITCH_CAC_AG PITCH_CAC_AJ PITCH_CAC_AN PITCH_CAC_AQ PITCH_CAC_AT PITCH_CAC_AW PITCH_CAC_AZ  PITCH_CAC_BY PITCH_CAC_CB PITCH_CAC_CE PITCH_CAC_CH
PITCH_CAC_AB PITCH_CAC_AE PITCH_CAC_AH PITCH_CAC_AL PITCH_CAC_AO PITCH_CAC_AR PITCH_CAC_AU PITCH_CAC_AX PITCH_CAC_BW  PITCH_CAC_BZ PITCH_CAC_CC PITCH_CAC_CF
PITCH_CAC_AC PITCH_CAC_AF PITCH_CAC_AI PITCH_CAC_AM PITCH_CAC_AP PITCH_CAC_AS PITCH_CAC_AV PITCH_CAC_AY PITCH_CAC_BX  PITCH_CAC_CA PITCH_CAC_CD PITCH_CAC_CG

$ ls PITCH_CAC_AA
PITCH_CAC_AA.OSDC_1_.CH0.fastq.gz   PITCH_CAC_AA.OSDC_1_.CH14.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH19.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH3.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH8.fastq.gz
PITCH_CAC_AA.OSDC_1_.CH10.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH15.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH1.fastq.gz   PITCH_CAC_AA.OSDC_1_.CH4.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH9.fastq.gz
PITCH_CAC_AA.OSDC_1_.CH11.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH16.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH20.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH5.fastq.gz
PITCH_CAC_AA.OSDC_1_.CH12.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH17.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH21.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH6.fastq.gz
PITCH_CAC_AA.OSDC_1_.CH13.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH18.fastq.gz  PITCH_CAC_AA.OSDC_1_.CH2.fastq.gz   PITCH_CAC_AA.OSDC_1_.CH7.fastq.gz
```
Now, we move data in a new dir:

```sh
$ cd /genoscope2/Data/
$ mkdir Droplets_chunked
$ mv /genoscope2/Results/Droplets/LSA_None_qcT_fuF/samples/PITCH_CAC_*/ Droplets_chunked/
```
and remove the merging project's dir:
```sh
$ rm -r /genoscope2/Results/Droplets/
```

Now, we can process the chunked dataset:

```sh
$ cd  /env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/
$ python SLURM/create_lsa_jobs.py -p adam_demo_Pitch -j Master
new Master SLURM job was successfully created in file:
/genoscope2/Results/Droplets/LSA_hyper_k31_h30_qcT_fuF/scripts/Master.slurm.job
$ sbatch /genoscope2/Results/Droplets/LSA_hyper_k31_h30_qcT_fuF/scripts/Master.slurm.job
Submitted batch job 90462
```

done?
check results into:
```sh
$ ls /genoscope2/Results/Droplets/LSA_hyper_k31_h30_qcT_fuF/logs/
$ ls /genoscope2/Results/Droplets/LSA_hyper_k31_h30_qcT_fuF/partitions/
$ ls /genoscope2/Results/Droplets/LSA_hyper_k31_h30_qcT_fuF/reports/
``` 

# LshSnn
Now, we can analyse our results in fasta or fna files. To do that we use our own clasterization method LshSnn (Locality Sensitive Hashing and Shared Nearest Neighbors).

Let's process all fasta files in the current folder.To do we run the script:

```sh
suffix=.fna

for fl in *.fna; 
do 
echo $fl
cr=$(echo "$fl" | sed -e "s/$suffix$//")
echo $cr
done

module load gnu/4.9.2

for fl in *.fna; 
do 
echo $fl
cr=$(echo "$fl" | sed -e "s/$suffix$//")
echo $cr
mkdir -p scaffolds_$cr/bin
/env/cns/proj/ADAM/Sawsan/Codes/LSHSnnReal/LshSnn $fl ">NODE" $fl.txt scaffolds_$cr/bin 24 100 0.2 0.2 0.3 0.7 300
done

```