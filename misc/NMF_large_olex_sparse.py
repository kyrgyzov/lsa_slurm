#!/usr/bin/env python

import sys, getopt
import glob, os
import numpy as np
import spams
import multiprocessing

#import time

n = 10
cpu = 42
clusters_nb = 1
thres = 0
wd = ""
hash_size = 30
atom_number = 1000


prefix = "/env/cns/bigtmp1/okyrgyzo/Virtual_10_g136/LSA_hyper_k33_h30/hashes/MT20in-40sp-400bp-10X-S10_I"

########### Load matrix
print("load matrix...")
values = {}
nzis = {}
nbr_H = 0
for k in range(n):
	print "read ligne ",k
	nzis[k] = np.memmap(prefix + str(k+1) + ".count.hash.nzi", dtype='uint64', mode='r')
	values[k] = np.memmap(prefix + str(k+1) + ".count.hash", dtype='int16', mode='r')
	lmx = int(nzis[k].max())
	if lmx > nbr_H:
		nbr_H = lmx
	print lmx,nbr_H
nbr_H += 1
print 'nbr_H',nbr_H

#global_nzi = np.zeros(nbr_H, dtype = np.bool)
norm = np.zeros(nbr_H, dtype = np.float64)
mean = np.zeros(nbr_H, dtype = np.float64)

for k in range(n):
	print "read ligne ",k
	#global_nzi[nzis[k]] = True
	norm[nzis[k]] += values[k].astype(np.float64) ** 2
	mean[nzis[k]] += values[k].astype(np.float64)

# Number of non zeros columns
#nz = np.sum(global_nzi)
#print nz
#inverted_index = np.cumsum(global_nzi) - 1
#print inverted_index

# Compute norm and mean for scaling

mean = mean/n
norm = norm - n * mean ** 2
norm = np.sqrt(norm)
#norm[norm == 0] = 1
#print np.sum(norm == 0)
print("matrix loaded")


############## Dictionary learning


# Divide matrix in div parts for avoiding Segfault from spams
div = int(nbr_H / (atom_number * 30000))
chunk_size = int(nbr_H/div)

print 'div',div
print 'chunk_size',chunk_size

sys.exit()


param = { 'K' : atom_number, 
          'lambda1' : 0.1, 
          'lambda2' : 0.05, 
          'posAlpha' : False, 'numThreads' : cpu, 'batchsize' : 10000,
          'iter' : 10000, 'posD' : False}


D = None

sections = np.arange(div)
#global_inds = np.arange(2**hash_size)[global_nzi]
global_inds = np.arange(nbr_H)



for k in sections[0:div]:
	param['D'] = D
	
	# Compute matrix
	sup = min(nbr_H, chunk_size * (k + 1))
	inds = global_inds[chunk_size * k : sup]
	size_of_matrix = sup - chunk_size * k
	abundance_matrix = np.zeros((n, size_of_matrix), dtype = np.float32)
	

	for i in range(n):
		chunk = np.zeros(size_of_matrix)
		inds_of_values = (nzis[i] >= global_inds[chunk_size * k]) * (nzis[i] < global_inds[sup])
		inds_absolute = nzis[i][inds_of_values]
		chunk[inverted_index[inds_absolute] - inverted_index[global_inds[chunk_size * k]]] = values[i][inds_of_values]
		abundance_matrix[i, :] = (chunk - mean[inds])/norm[inds]

	
	# Perform Dictionary learning
	X = np.asfortranarray( abundance_matrix )
	D = spams.trainDL(X, **param)


# Save dictionary
np.save("matrices/D", D)




def _extract_lasso_param(f_param):
    lst = [ 'L','lambda1','lambda2','mode','pos', 'ols','numThreads','length_path','verbose','cholesky']
    l_param = {'return_reg_path' : False}
    if 'posAlpha' in f_param:
    	l_param['pos'] = f_param['posAlpha']
    for x in lst:
        if x in f_param:
            l_param[x] = f_param[x]
    return l_param


# lparam is same as param but for lasso
lparam = _extract_lasso_param(param)
lparam['numThreads'] = 1

############ Compute code matrix


chunk_size = 2**17
iter_nb = int(nbr_H/chunk_size + 1)
alpha = np.zeros((nbr_H, clusters_nb))


def write_part(k):

	# Compute matrix
	sup = min(nbr_H - 1, (k + 1) * chunk_size)
	inds = global_inds[chunk_size * k : sup]
	size_of_matrix = sup - chunk_size * k
	abundance_matrix = np.zeros((n, size_of_matrix), dtype = np.float32)

	for i in range(n):
		chunk = np.zeros(size_of_matrix)
		inds_of_values = (nzis[i] >= global_inds[chunk_size * k]) * (nzis[i] < global_inds[sup])
		inds_absolute = nzis[i][inds_of_values]
		chunk[inverted_index[inds_absolute] - inverted_index[global_inds[chunk_size * k]]] = values[i][inds_of_values]
		abundance_matrix[i, :] = (chunk - mean[inds])/norm[inds]


	# Perform lasso
	x = np.asfortranarray(abundance_matrix)
	a = spams.lasso(x, D = D, **lparam).toarray()

	
	# Apply thershold on
	clusters = np.argsort(a, axis = 0)[-clusters_nb:][::-1]
	mask = a[clusters,  np.arange(size_of_matrix)]
	mask = mask > thres
	clusters[~mask] = -1

	return clusters + 1


p = multiprocessing.Pool(cpu)
r = p.imap(write_part, range(0, iter_nb))

for k in range(iter_nb):
	sup = min(nbr_H - 1, (k + 1) * chunk_size)
	alpha[:clusters_nb, k * chunk_size:sup] = r.next()[:]


# Save results
np.save('matrices/hash_cluster', alpha)


