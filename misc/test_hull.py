import sys,random
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as la
from scipy.spatial import ConvexHull

sys.path.insert(0, '/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/LSA')
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D

npt = 100

pt = np.random.rand(npt, 2)   # 30 random points in 2-D

plt.figure()
plt.plot(pt[:,0], pt[:,1], 'o')

fl= np.ones(pt.shape[0],dtype=bool)


while np.sum(fl)>2:
    print 'iter ---------------'
    tpt=pt[fl,:]
    ind = np.nonzero(fl)[0]
    hull = ConvexHull(tpt)
    cl=np.random.rand(3)
    for simplex in hull.simplices:
        plt.plot(tpt[simplex, 0], tpt[simplex, 1], '-',c=cl)
    fl[ind[hull.vertices]]=0


pt = np.random.rand(npt, 3)   # 30 random points in 2-D

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(pt[:,0], pt[:,1],pt[:,2],'o', label='parametric curve')

fl= np.ones(pt.shape[0],dtype=bool)

while np.sum(fl)>3:
    print 'iter ---------------'
    tpt=pt[fl,:]
    ind = np.nonzero(fl)[0]
    hull = ConvexHull(tpt)
    cl=np.random.rand(3)
    for simplex in hull.simplices:
        #print simplex
        ax.plot(tpt[simplex[[0,1]], 0], tpt[simplex[[0,1]], 1], tpt[simplex[[0,1]], 2], '-',c=cl)
        ax.plot(tpt[simplex[[0,2]], 0], tpt[simplex[[0,2]], 1], tpt[simplex[[0,2]], 2], '-',c=cl)
        ax.plot(tpt[simplex[[2,1]], 0], tpt[simplex[[2,1]], 1], tpt[simplex[[2,1]], 2], '-',c=cl)
    fl[ind[hull.vertices]]=0
    plt.title(str(np.sum(fl==0)))

    #break



fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(pt[:,0], pt[:,1],pt[:,2],'o')
fl= np.ones(pt.shape[0],dtype=bool)

sz = pt.shape

it = 0
lr = sz[0]*np.ones(sz[0],dtype=int)

while np.sum(fl)>3:
    print 'iter ---------------'
    print sz
    tpt=pt[fl,:]
    ind = np.nonzero(fl)[0]
    cl=np.random.rand(3)
    for i in range(sz[1]-1):
        for j in range(i+1,sz[1]):

            print i,j
            #cl=np.random.rand(3)
            thull = ConvexHull(tpt[:,[i,j]])
            lr[ind[thull.vertices]] =it
            for simplex in thull.simplices:
                #print simplex
                ax.plot(tpt[simplex, 0], tpt[simplex, 1], tpt[simplex, 2], '-',c=cl)
            #print thull.vertices
            #sys.exit()
            
    #print lr
    tf = lr ==it
    fl[tf]=0

    #ax.plot(pt[tf,0], pt[tf,1],pt[tf,2],'or')
    plt.title(str(np.sum(tf)))

    #break


plt.show()
