#!/bin/bash

module load pigz
module load bedtools/2.26.1
module load samtools

p2cip=/env/cns/proj/projet_BWH/scratch/EGAD00001001991/
p2fastq=/env/cns/bigtmp1/okyrgyzo/EGAD00001001991_fastq/

mkdir -p $p2fastq

for fl in $p2cip/*.cip
do
    tname=${fl##*/}
    tname=`echo $tname | cut -c2-`
    tname=$(echo "$tname" | tr '_' '.')
    tname="${tname%%.*}"
    if [ -f $p2fastq$tname.fastq.gz ]
    then
    continue
    fi

echo ${tname}
#ln -s ${fl} $p2fastq$tname.bam.cip
#java -jar /genoscope2/Code/cip/EGA/EgaDemoClient.jar -pf /genoscope2/Code/cip/LLDeep/.ega -dc $p2fastq$tname.bam.cip -dck xyz
#/software/anaconda2/bin/samtools fastq $p2fastq$tname.bam > $p2fastq$tname.fastq &
#rm $p2gz$tname.bam
#pigz $p2fastq$tname.fastq &

ln -s ${fl} $p2fastq$tname.bam.cip
java -jar /env/cns/proj/projet_BWH/scratch/EGA/EgaDemoClient.jar -pf /env/cns/proj/ADAM/LLDeep/.ega -dc $p2fastq$tname.bam.cip -dck xyz
samtools fastq $p2fastq$tname.bam > $p2fastq$tname.fastq
rm $p2fastq$tname.bam
pigz $p2fastq$tname.fastq

done
