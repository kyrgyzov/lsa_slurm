import sys,random
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as la
from collections import defaultdict
from scipy.spatial.distance import cdist
sys.path.insert(0, '/env/cns/proj/ADAM/BWG/LSA/LatentStrainAnalysis/LSA')
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D
from scipy.sparse import dok_matrix
from space_m import dc_from_2d,find_nnbr_connects


npt = 50
m0=np.random.normal(0,1,(npt,3))
m1=np.random.normal(0,1,(npt,3))+np.array([0,15,0])
m2=np.random.normal(0,1,(npt,3))+np.array([0,0,15])
#pt = np.hstack((m0.T,m1.T,m2.T))   # 30 random points in 2-D
pt = np.hstack((m0.T,m1.T))   # 30 random points in 2-D


sz = pt.shape
nnbr = 2*sz[0]
print sz,nnbr

DC = dc_from_2d(pt,nnbr)

for i in range(len(DC)):
    arr = [len(e[1]) for e in DC[i].iteritems()]
    print i,len(DC[i]),arr

print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'    

DC = find_nnbr_connects(DC,pt,nnbr)
for i in range(len(DC)):
    arr = [len(e[1]) for e in DC[i].iteritems()]
    print i,len(DC[i]),arr

print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'    
#sys.exit()
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(pt[0,:], pt[1,:],pt[2,:],'o')
for l in range(len(DC)):
    cl=np.random.rand(3)
    for cd in DC[l].iteritems():
        kv = cd[0]
        for vl in cd[1]:
            ta = np.array([kv,vl],dtype=int)
            ax.plot(pt[0,ta], pt[1,ta], pt[2,ta], '-',c=cl)

plt.show()
